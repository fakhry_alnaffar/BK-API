import 'package:bio_clean/api/api_model/user_model.dart';
import 'package:bio_clean/getx_controller/user_getx_controller.dart';
import 'package:bio_clean/responsive/size_config.dart';
import 'package:bio_clean/utils/helpers.dart';
import 'package:bio_clean/widgets/app_text_filed.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class AddEmployeeScreen extends StatefulWidget {
  @override
  _AddMeetState createState() => _AddMeetState();
}

class _AddMeetState extends State<AddEmployeeScreen> with Helpers {
  late var _nameController;
  late var _emailController;
  late var _phoneController;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _nameController = TextEditingController();
    _emailController = TextEditingController();
    _phoneController = TextEditingController();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _nameController.dispose();
    _emailController.dispose();
    _phoneController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().designWidth(4.14).designHeight(8.96).init(context);
    return SafeArea(
      child: Scaffold(
        extendBodyBehindAppBar: true,
        appBar: AppBar(
          centerTitle: true,
          elevation: 0,
          leading: Container(
              margin: EdgeInsetsDirectional.only(
                  top: SizeConfig().scaleWidth(20),
                  bottom: SizeConfig().scaleWidth(10),
                  start: SizeConfig().scaleWidth(5)),
              child: IconButton(
                icon: Icon(Icons.arrow_back_ios_rounded),
                onPressed: () {
                  Navigator.pop(context);
                },
              )),
          backgroundColor: Colors.transparent,
          title: Container(
            margin: EdgeInsetsDirectional.only(
                top: SizeConfig().scaleWidth(20),
                bottom: SizeConfig().scaleWidth(10),
                end: SizeConfig().scaleWidth(0)),
            child: Text(
              'اضافة موظف',
              style: TextStyle(
                  letterSpacing: SizeConfig().scaleWidth(2),
                  wordSpacing: SizeConfig().scaleWidth(0.5),
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: SizeConfig().scaleWidth(24)),
            ),
          ),
        ),
        body: Stack(
          children: [
            Container(
              width: double.infinity,
              height: double.infinity,
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: AlignmentDirectional.topStart,
                  end: AlignmentDirectional.bottomEnd,
                  colors: [
                    Color(0xffED2424),
                    Colors.white,
                  ],
                ),
              ),
            ),
            SizedBox(
              height: SizeConfig().scaleHeight(140),
            ),
            Align(
              child: Container(
                margin: EdgeInsets.only(top: SizeConfig().scaleHeight(100)),
                width: double.infinity,
                height: double.infinity,
                alignment: Alignment.bottomCenter,
                decoration: BoxDecoration(
                  color: Color(0xffF0F4FD),
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(SizeConfig().scaleWidth(40)),
                    topRight: Radius.circular(SizeConfig().scaleWidth(40)),
                  ),
                ),
                child: Padding(
                  padding: EdgeInsets.all(SizeConfig().scaleWidth(24)),
                  child: Align(
                    alignment: Alignment.topCenter,
                    child: SingleChildScrollView(
                      physics: BouncingScrollPhysics(),
                      child: Column(
                        children: [
                          SizedBox(
                            height: SizeConfig().scaleHeight(40),
                          ),
                          AppTextFiled(
                            labelText: 'اسم الموظف',
                            controller: _nameController,
                            prefix: Icons.person_outline_rounded,
                          ),
                          SizedBox(
                            height: SizeConfig().scaleHeight(20),
                          ),
                          AppTextFiled(
                            textInputType: TextInputType.number,
                            labelText: 'رقم الهاتف',
                            controller: _phoneController,
                            prefix: Icons.phone_enabled_outlined,
                          ),
                          SizedBox(
                            height: SizeConfig().scaleHeight(20),
                          ),
                          AppTextFiled(
                            textInputType: TextInputType.emailAddress,
                            labelText: 'البريد الاكتروني',
                            controller: _emailController,
                            prefix: Icons.email_outlined,
                          ),
                          SizedBox(
                            height: SizeConfig().scaleHeight(30),
                          ),
                          ElevatedButton(
                            style: ElevatedButton.styleFrom(
                              minimumSize: Size(double.infinity,
                                  SizeConfig().scaleHeight(60)),
                              primary: Color(0xffED2424),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10),
                              ),
                            ),
                            onPressed: () async {
                              await perfCreateNewUser();
                            },
                            child: Text(
                              'تاكيد العملية',
                              style: TextStyle(
                                  fontSize: SizeConfig().scaleTextFont(20)),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<void> perfCreateNewUser() async {
    if (checkData()) {
      await crateUser();
    } else {}
  }

  bool checkData() {
    if (_nameController.text.isNotEmpty &&
        _emailController.text.isNotEmpty &&
        _phoneController.text.isNotEmpty) {
      return true;
    }
    showSnackBar(context: context, message: 'Enter Required Data', error: true);
    return false;
  }

  Future<void> crateUser() async {
    bool crated =
        await UserGetsController.to.crateUser(context: context, user: user);
    if (crated) {
      clear();
      // showSnackBar(context: context, message: 'TASK ADDED SUCCESSFULLY');
      Navigator.pop(context);
    }
    // Navigator.pushReplacementNamed(context, 'main_screen');
  }

  UserData get user {
    UserData user = UserData();
    user.name = _nameController.text;
    user.email = _emailController.text;
    user.phone = _phoneController.text;
    user.password = '123123';
    user.image =
        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRPtENZS1gdjgiNbiX6gLEZPDCiKUXSkSo9SY4ZnWPxGwESsCxZeoXUOIQiFsD8ph-WmAc&usqp=CAU';
    return user;
  }

  void clear() {
    _nameController.text = '';
    _emailController.text = '';
    _phoneController.text = '';
  }
}
//0xff5A55CA
