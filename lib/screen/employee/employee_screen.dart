import 'package:bio_clean/components/component.dart';
import 'package:bio_clean/getx_controller/user_getx_controller.dart';
import 'package:bio_clean/responsive/size_config.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/src/rx_flutter/rx_getx_widget.dart';

import 'edit_employee.dart';

class EmployeeScreen extends StatefulWidget {
  @override
  _EmployeeScreenState createState() => _EmployeeScreenState();
}

class _EmployeeScreenState extends State<EmployeeScreen> {
  UserGetsController _userGetsController = Get.put(UserGetsController());

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _userGetsController.getUser();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().designWidth(4.14).designHeight(8.96).init(context);
    return SafeArea(
      child: Scaffold(
        extendBodyBehindAppBar: true,
        appBar: AppBar(
          leading: Container(
              margin: EdgeInsetsDirectional.only(
                  top: SizeConfig().scaleWidth(0),
                  bottom: SizeConfig().scaleWidth(0),
                  start: SizeConfig().scaleWidth(5)),
              child: IconButton(
                icon: Icon(Icons.arrow_back_ios_rounded),
                onPressed: () {
                  Navigator.pushNamed(context, 'home_screen');
                },
              )),          centerTitle: true,
          elevation: 0,
          backgroundColor: Colors.transparent,
          title: Container(
            margin: EdgeInsetsDirectional.only(
                top: SizeConfig().scaleWidth(20),
                bottom: SizeConfig().scaleWidth(0),
                end: SizeConfig().scaleWidth(0)),
            child: Text(
              'الموظفين',
              style: TextStyle(
                  letterSpacing: 2,
                  wordSpacing: 0.5,
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: SizeConfig().scaleWidth(24)),
            ),
          ),
        ),
        body: Stack(children: [
          Container(
            clipBehavior: Clip.antiAlias,
            width: double.infinity,
            height: double.infinity,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: AlignmentDirectional.topStart,
                end: AlignmentDirectional.bottomEnd,
                colors: [
                  Color(0xffED2424),
                  Colors.white,
                ],
              ),
            ),
          ),
          SizedBox(
            height: SizeConfig().scaleHeight(140),
          ),
          Align(
              child: Container(
            clipBehavior: Clip.antiAlias,
            margin: EdgeInsets.only(top: SizeConfig().scaleHeight(100)),
            padding: EdgeInsetsDirectional.only(top: 0),
            width: double.infinity,
            height: double.infinity,
            alignment: Alignment.bottomCenter,
            decoration: BoxDecoration(
              color: Color(0xffF0F4FD),
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(SizeConfig().scaleWidth(40)),
                topRight: Radius.circular(SizeConfig().scaleWidth(40)),
              ),
            ),
            child: GetX<UserGetsController>(
                builder: (UserGetsController controller) {
              if (controller.users.isNotEmpty) {
                return ListView.separated(
                  physics: BouncingScrollPhysics(),
                  clipBehavior: Clip.antiAlias,
                  shrinkWrap: false,
                  padding: EdgeInsetsDirectional.only(top: 45),
                  itemBuilder: (context, index) {
                    return Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      child: InkWell(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => EditEmployeeScreen(
                                      user: controller.users[index])));
                        },
                        child: showEmployee(
                          title: controller.users[index].name.toString(),
                          subtitle: controller.users[index].email.toString(),
                          image:
                              'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRPtENZS1gdjgiNbiX6gLEZPDCiKUXSkSo9SY4ZnWPxGwESsCxZeoXUOIQiFsD8ph-WmAc&usqp=CAU',
                          widgetDelete: IconButton(
                              onPressed: () {
                                deleteUser(index: index);
                              },
                              icon: Icon(
                                Icons.delete,
                                color: Colors.redAccent,
                              )),
                          context: context,
                        ),
                      ),
                    );
                  },
                  separatorBuilder: (BuildContext context, int index) {
                    return SizedBox(
                      height: SizeConfig().scaleHeight(14),
                    );
                  },
                  itemCount: controller.users.length,
                );
              } else {
                return Center(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Icon(
                        Icons.warning,
                        size: 80,
                        color: Colors.grey.shade500,
                      ),
                      Text(
                        'NO Users',
                        style: TextStyle(
                          color: Colors.grey.shade500,
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ],
                  ),
                );
              }
            }),
          ))
        ]),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            Navigator.pushNamed(context, 'add_employee');
          },
          child: Icon(Icons.add),
          backgroundColor: Color(0xffED2424),
        ),
      ),
    );
  }

  Future<void> deleteUser({required int index}) async {
    await UserGetsController.to.deleteUser(context: context, index: index);
  }
}
