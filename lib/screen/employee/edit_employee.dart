import 'package:bio_clean/api/api_model/meet_model.dart';
import 'package:bio_clean/api/api_model/user_model.dart';
import 'package:bio_clean/getx_controller/meet_getx_controller.dart';
import 'package:bio_clean/getx_controller/user_getx_controller.dart';
import 'package:bio_clean/responsive/size_config.dart';
import 'package:bio_clean/utils/helpers.dart';
import 'package:bio_clean/widgets/app_text_filed.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class EditEmployeeScreen extends StatefulWidget {
  late final UserData user;

  EditEmployeeScreen({required this.user});
  @override
  _AddMeetState createState() => _AddMeetState();
}

class _AddMeetState extends State<EditEmployeeScreen> with Helpers {
  late TextEditingController _name = TextEditingController();
  late TextEditingController _email = TextEditingController();
  late TextEditingController _password = TextEditingController();
  late TextEditingController _phone = TextEditingController();


  @override
  void initState() {
    // TODO: implement initState
    _name = TextEditingController(text: widget.user.name);
    _email = TextEditingController(text: widget.user.email);
    _password = TextEditingController(text: widget.user.createdAt.trimRight().toString());
    _phone = TextEditingController(text: widget.user.phone.toString());
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _name.dispose();
    _email.dispose();
    _password.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().designWidth(4.14).designHeight(8.96).init(context);
    return SafeArea(
      child: Scaffold(
        extendBodyBehindAppBar: true,
        appBar: AppBar(

          centerTitle: true,
          elevation: 0,
          leading: Container(
              margin: EdgeInsetsDirectional.only(
                  top: SizeConfig().scaleWidth(20),
                  bottom: SizeConfig().scaleWidth(10),
                  start: SizeConfig().scaleWidth(5)),
              child: IconButton(
                icon: Icon(Icons.arrow_back_ios_rounded),
                onPressed: () {
                  Navigator.pop(context);
                },
              )),
          backgroundColor: Colors.transparent,
          title: Container(
            margin: EdgeInsetsDirectional.only(
                top: SizeConfig().scaleWidth(20),
                bottom: SizeConfig().scaleWidth(10),
                end: SizeConfig().scaleWidth(0)),
            child: Text(
              'عرض بيانات الموظف',
              style: TextStyle(
                  letterSpacing: SizeConfig().scaleWidth(2),
                  wordSpacing: SizeConfig().scaleWidth(0.5),
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: SizeConfig().scaleWidth(24)),
            ),
          ),
        ),
        body: Stack(
          children: [
            Container(
              width: double.infinity,
              height: double.infinity,
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: AlignmentDirectional.topStart,
                  end: AlignmentDirectional.bottomEnd,
                  colors: [
                    Color(0xffED2424),
                    Colors.white,
                  ],
                ),
              ),
            ),
            SizedBox(
              height: SizeConfig().scaleHeight(140),
            ),
            Align(
              child: Container(
                margin: EdgeInsets.only(top: SizeConfig().scaleHeight(100)),
                width: double.infinity,
                height: double.infinity,
                alignment: Alignment.bottomCenter,
                decoration: BoxDecoration(
                  color: Color(0xffF0F4FD),
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(SizeConfig().scaleWidth(40)),
                    topRight: Radius.circular(SizeConfig().scaleWidth(40)),
                  ),
                ),
                child: Padding(
                  padding: EdgeInsets.all(SizeConfig().scaleWidth(24)),
                  child: Align(
                    alignment: Alignment.topCenter,
                    child: SingleChildScrollView(
                      physics: BouncingScrollPhysics(),
                      child: Column(
                        children: [
                          SizedBox(
                            height: SizeConfig().scaleHeight(40),
                          ),
                          AppTextFiled(
                            labelText: 'اسم الموظف',
                            controller: _name,
                            prefix: Icons.person_outline_rounded,
                          ),
                          SizedBox(
                            height: SizeConfig().scaleHeight(20),
                          ),
                          AppTextFiled(
                            labelText: 'ايميل الموظف',
                            controller: _email,
                            prefix: Icons.email_outlined,
                          ),
                          SizedBox(
                            height: SizeConfig().scaleHeight(20),
                          ),
                          AppTextFiled(
                            labelText: 'رقم الهاتف',
                            controller: _phone,
                            prefix: Icons.phone_enabled_outlined,
                          ),
                          SizedBox(
                            height: SizeConfig().scaleHeight(30),
                          ),
                          ElevatedButton(
                            style: ElevatedButton.styleFrom(
                              minimumSize: Size(double.infinity,
                                  SizeConfig().scaleHeight(60)),
                              primary: Color(0xffED2424),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10),
                              ),
                            ),
                            onPressed: () async{
                              await perfUpdateUser();
                            },
                            child: Text(
                              'تاكيد العملية',
                              style: TextStyle(
                                  fontSize: SizeConfig().scaleTextFont(20)),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<void> perfUpdateUser() async {
    if (checkData()) {
      await updateUser();
    } else {}
  }

  bool checkData() {
    if (_name.text.isNotEmpty&&_email.text.isNotEmpty) {
      return true;
    }
    showSnackBar(context: context, message: 'Enter Required Data', error: true);
    return false;
  }

  Future<void> updateUser() async {
    bool updated =
    await UserGetsController.to.updateUser(context: context, user: user);
    if (updated) {
      Navigator.pop(context);
    }
  }


  UserData get user {
    UserData user = widget.user;
    user.id = widget.user.id;
    user.name = _name.text;
    user.email = _email.text;
    user.phone = _phone.text;
    user.password = '_password.text';
    user.image = '';
    return user;
  }

}
//0xff5A55CA
