import 'package:bio_clean/components/component.dart';
import 'package:bio_clean/getx_controller/import_task_getx_controller.dart';
import 'package:bio_clean/getx_controller/meet_getx_controller.dart';
import 'package:bio_clean/responsive/size_config.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/src/rx_flutter/rx_getx_widget.dart';

import 'edit_import_task.dart';

class ImportScreen extends StatefulWidget {
  @override
  _ImportScreenState createState() => _ImportScreenState();
}

class _ImportScreenState extends State<ImportScreen> {
  ImportTaskGetsController _meetGetsController = Get.put(ImportTaskGetsController());
@override
  void initState() {
    // TODO: implement initState
    super.initState();
    _meetGetsController.getImportTask();
  }
  @override
  Widget build(BuildContext context) {
    SizeConfig().designWidth(4.14).designHeight(8.96).init(context);
    return SafeArea(
      child: Scaffold(
        extendBodyBehindAppBar: true,
        appBar: AppBar(
          leading: Container(
              margin: EdgeInsetsDirectional.only(
                  top: SizeConfig().scaleWidth(0),
                  bottom: SizeConfig().scaleWidth(0),
                  start: SizeConfig().scaleWidth(5)),
              child: IconButton(
                icon: Icon(Icons.arrow_back_ios_rounded),
                onPressed: () {
                  Navigator.pushNamed(context, 'home_screen');
                },
              )),          centerTitle: true,
          elevation: 0,
          backgroundColor: Colors.transparent,
          title: Container(
            margin: EdgeInsetsDirectional.only(
                top: SizeConfig().scaleWidth(20),
                bottom: SizeConfig().scaleWidth(0),
                end: SizeConfig().scaleWidth(0)),
            child: Text(
              'مهام الاستيراد',
              style: TextStyle(
                  letterSpacing: 2,
                  wordSpacing: 0.5,
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: SizeConfig().scaleWidth(24)),
            ),
          ),
        ),
        body: Stack(children: [
          Container(
            clipBehavior: Clip.antiAlias,
            width: double.infinity,
            height: double.infinity,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: AlignmentDirectional.topStart,
                end: AlignmentDirectional.bottomEnd,
                colors: [
                  Color(0xffED2424),
                  Colors.white,
                ],
              ),
            ),
          ),
          SizedBox(
            height: SizeConfig().scaleHeight(140),
          ),
          Align(
              child: Container(
            clipBehavior: Clip.antiAlias,
            margin: EdgeInsets.only(top: SizeConfig().scaleHeight(100)),
            padding: EdgeInsetsDirectional.only(top: 0),
            width: double.infinity,
            height: double.infinity,
            alignment: Alignment.bottomCenter,
            decoration: BoxDecoration(
              color: Color(0xffF0F4FD),
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(SizeConfig().scaleWidth(40)),
                topRight: Radius.circular(SizeConfig().scaleWidth(40)),
              ),
            ),
            child: GetX<ImportTaskGetsController>(
                builder: (ImportTaskGetsController controller) {
              if (controller.imports.isNotEmpty) {
                return ListView.separated(
                  physics: BouncingScrollPhysics(),
                  clipBehavior: Clip.antiAlias,
                  shrinkWrap: false,
                  padding: EdgeInsetsDirectional.only(top: 45),
                  itemBuilder: (context, index) {
                    return Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      child: InkWell(
                        onTap: (){
                          // Navigator.push(
                          //     context,
                          //     MaterialPageRoute(
                          //         builder: (context) => EditMeetScreen(
                          //             meet: controller.meets[index])));
                        },
                        child: showWidgetMeet(
                            onPressed: () => Navigator.pop(context),
                            status: 'قيد العمل',
                            title: controller.imports[index].name.toString(),
                            description:
                                controller.imports[index].description.toString(),
                            time: controller.imports[index].time.toString(),
                            date: controller.imports[index].date.toString(),
                            onDelete: IconButton(
                              onPressed: () async {
                                await deleteTask(index: index);
                                print(index);
                              },
                              icon: Icon(Icons.delete_rounded,
                                  size: SizeConfig().scaleHeight(26),
                                  color: Colors.red.shade600),
                            )
                        ),
                      ),
                    );
                  },
                  separatorBuilder: (BuildContext context, int index) {
                    return SizedBox(
                      height: SizeConfig().scaleHeight(14),
                    );
                  },
                  itemCount: controller.imports.length,
                );
              } else {
                return Center(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Icon(
                        Icons.warning,
                        size: 80,
                        color: Colors.grey.shade500,
                      ),
                      Text(
                        'NO Import Task',
                        style: TextStyle(
                          color: Colors.grey.shade500,
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ],
                  ),
                );
              }
            }),
          ))
        ]),
        // floatingActionButton: FloatingActionButton(
        //   onPressed: () {
        //     Navigator.pushNamed(context, 'add_meet');
        //   },
        //   child: Icon(Icons.add),
        //   backgroundColor: Color(0xffED2424),
        // ),
      ),
    );
  }

  Future<void> deleteTask({required int index}) async {
    await MeetGetsController.to.deleteMeet(context: context, index: index);
  }
}
