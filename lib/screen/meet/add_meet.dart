import 'package:bio_clean/api/api_model/meet_model.dart';
import 'package:bio_clean/getx_controller/meet_getx_controller.dart';
import 'package:bio_clean/responsive/size_config.dart';
import 'package:bio_clean/utils/helpers.dart';
import 'package:bio_clean/widgets/app_text_filed.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class AddMeetScreen extends StatefulWidget {
  @override
  _AddMeetState createState() => _AddMeetState();
}

class _AddMeetState extends State<AddMeetScreen> with Helpers {
  late var _meetNameController;
  late var _meetTimeController;
  late var _meetDateController;
  late var _meetDescriptionController;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _meetNameController = TextEditingController();
    _meetTimeController = TextEditingController();
    _meetDateController = TextEditingController();
    _meetDescriptionController = TextEditingController();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _meetNameController.dispose();
    _meetTimeController.dispose();
    _meetDateController.dispose();
    _meetDescriptionController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().designWidth(4.14).designHeight(8.96).init(context);
    return SafeArea(
      child: Scaffold(
        extendBodyBehindAppBar: true,
        appBar: AppBar(
          centerTitle: true,
          elevation: 0,
          leading: Container(
              margin: EdgeInsetsDirectional.only(
                  top: SizeConfig().scaleWidth(20),
                  bottom: SizeConfig().scaleWidth(10),
                  start: SizeConfig().scaleWidth(5)),
              child: IconButton(
                icon: Icon(Icons.arrow_back_ios_rounded),
                onPressed: () {
                  Navigator.pop(context);
                },
              )),
          backgroundColor: Colors.transparent,
          title: Container(
            margin: EdgeInsetsDirectional.only(
                top: SizeConfig().scaleWidth(20),
                bottom: SizeConfig().scaleWidth(10),
                end: SizeConfig().scaleWidth(0)),
            child: Text(
              'اضافة اجتماع',
              style: TextStyle(
                  letterSpacing: SizeConfig().scaleWidth(2),
                  wordSpacing: SizeConfig().scaleWidth(0.5),
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: SizeConfig().scaleWidth(24)),
            ),
          ),
        ),
        body: Stack(
          children: [
            Container(
              width: double.infinity,
              height: double.infinity,
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: AlignmentDirectional.topStart,
                  end: AlignmentDirectional.bottomEnd,
                  colors: [
                    Color(0xffED2424),
                    Colors.white,
                  ],
                ),
              ),
            ),
            SizedBox(
              height: SizeConfig().scaleHeight(140),
            ),
            Align(
              child: Container(
                margin: EdgeInsets.only(top: SizeConfig().scaleHeight(100)),
                width: double.infinity,
                height: double.infinity,
                alignment: Alignment.bottomCenter,
                decoration: BoxDecoration(
                  color: Color(0xffF0F4FD),
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(SizeConfig().scaleWidth(40)),
                    topRight: Radius.circular(SizeConfig().scaleWidth(40)),
                  ),
                ),
                child: Padding(
                  padding: EdgeInsets.all(SizeConfig().scaleWidth(24)),
                  child: Align(
                    alignment: Alignment.topCenter,
                    child: SingleChildScrollView(
                      physics: BouncingScrollPhysics(),
                      child: Column(
                        children: [
                          SizedBox(
                            height: SizeConfig().scaleHeight(40),
                          ),
                          AppTextFiled(
                            labelText: 'اسم الاجتماع',
                            controller: _meetNameController,
                            prefix: Icons.title,
                          ),
                          SizedBox(
                            height: SizeConfig().scaleHeight(20),
                          ),
                          AppTextFiled(
                            validator: (String? value) {
                              if (value == null) {
                                return 'S';
                              }
                              return null;
                            },
                            textInputType: TextInputType.datetime,
                            labelText: 'وقت الاجتماع',
                            // isEnabled: false,
                            controller: _meetTimeController,
                            prefix: Icons.timer,
                            onTap: () {
                              showTimePicker(
                                context: context,
                                initialTime: TimeOfDay.now(),
                              )
                                  .then((value) => _meetTimeController.text =
                                      value!.format(context).toString())
                                  .catchError((error) => null);
                            },
                          ),
                          SizedBox(
                            height: SizeConfig().scaleHeight(20),
                          ),
                          AppTextFiled(
                            textInputType: TextInputType.datetime,
                            labelText: 'تاريخ الاجتماع',
                            // isEnabled: false,
                            controller: _meetDateController,
                            prefix: Icons.date_range_rounded,
                            onTap: () {
                              showDatePicker(
                                context: context,
                                initialDate: DateTime.now(),
                                firstDate: DateTime.now(),
                                lastDate:
                                    DateTime.now().add(Duration(days: 60)),
                              ).then((value) {
                                // dateController.text=value.toString();
                                //باقي الشغل
                                _meetDateController.text = DateFormat.yMMMd()
                                    .format(value!)
                                    .toString();
                                // print(DateFormat.yMMMd().format(value!));
                              });
                            },
                          ),
                          SizedBox(
                            height: SizeConfig().scaleHeight(20),
                          ),
                          AppTextFiled(
                            labelText: 'وصف الاجتماع',
                            controller: _meetDescriptionController,
                            prefix: Icons.description_rounded,
                          ),
                          SizedBox(
                            height: SizeConfig().scaleHeight(30),
                          ),
                          ElevatedButton(
                            style: ElevatedButton.styleFrom(
                              minimumSize: Size(double.infinity,
                                  SizeConfig().scaleHeight(60)),
                              primary: Color(0xffED2424),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10),
                              ),
                            ),
                            onPressed: () async{
                              await perfCreateNewTask();
                            },
                            child: Text(
                              'تاكيد العملية',
                              style: TextStyle(
                                  fontSize: SizeConfig().scaleTextFont(20)),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

    Future<void> perfCreateNewTask() async {
      if (checkData()) {
        await crateTask();
      } else {}
    }

    bool checkData() {
      if (_meetNameController.text.isNotEmpty &&
          _meetTimeController.text.isNotEmpty &&
          _meetDateController.text.isNotEmpty &&
          _meetDescriptionController.text.isNotEmpty) {
        return true;
      }
      showSnackBar(context: context, message: 'Enter Required Data', error: true);
      return false;
    }

    Future<void> crateTask() async {
      bool crated =
          await MeetGetsController.to.crateMeet(context: context, meet: meet);
      if (crated) {
        clear();
        // showSnackBar(context: context, message: 'TASK ADDED SUCCESSFULLY');
        Navigator.pop(context);
      }
      // Navigator.pushReplacementNamed(context, 'main_screen');
    }

    Meet get meet {
      Meet task = Meet();
      task.name = _meetNameController.text;
      task.description = _meetDescriptionController.text;
      task.time = _meetTimeController.text;
      task.date = _meetDateController.text;
      return task;
    }

    void clear() {
      _meetNameController.text = '';
      _meetDescriptionController.text = '';
      _meetTimeController.text = '';
      _meetDateController.text = '';
    }
}
//0xff5A55CA
