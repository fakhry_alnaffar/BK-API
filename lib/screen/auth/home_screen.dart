import 'package:bio_clean/api/api_controller/import_task_api_controller.dart';
import 'package:bio_clean/api/api_controller/meet_api_controller.dart';
import 'package:bio_clean/api/api_controller/task_api_controller.dart';
import 'package:bio_clean/api/api_controller/user_api_controller.dart';
import 'package:bio_clean/components/component.dart';
import 'package:bio_clean/getx_controller/meet_getx_controller.dart';
import 'package:bio_clean/getx_controller/task_getx_controller.dart';
import 'package:bio_clean/getx_controller/user_getx_controller.dart';
import 'package:bio_clean/models/dashboard_items.dart';
import 'package:bio_clean/responsive/size_config.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> with WidgetsBindingObserver {
  UserGetsController _userGetsController = Get.put(UserGetsController());
  MeetGetsController _meetGetsController = Get.put(MeetGetsController());
  TaskGetsController _taskGetsController = Get.put(TaskGetsController());
  List<DashboardItem> items = <DashboardItem>[];
  int _countMeet = 0;
  int _countTask = 0;
  int _countUser = 0;
  int _countImportTask = 0;

  @override
  void initState() {
    // TODO: implement initState
    WidgetsBinding.instance!.addObserver(this);
    listValue();
    getItemCount();
    _userGetsController.getUser();
    _meetGetsController.getMeet();
    _taskGetsController.getTask();
    setState(() {
      listValue();
      getItemCount();
      _userGetsController.getUser();
      _meetGetsController.getMeet();
      _taskGetsController.getTask();
    });
    _userGetsController.getUser();
    _meetGetsController.getMeet();
    _taskGetsController.getTask();
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    WidgetsBinding.instance!.removeObserver(this);
    listValue();
    _meetGetsController.getMeet();
    _userGetsController.getUser();
    _taskGetsController.getTask();
    getItemCount();
    setState(() {
      listValue();
      getItemCount();
      _userGetsController.getUser();
      _meetGetsController.getMeet();
      _taskGetsController.getTask();
    });
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed) {
      //do your stuff
      listValue();
      _userGetsController.getUser();
      _meetGetsController.getMeet();
      _taskGetsController.getTask();
      setState(() {
        listValue();
        _userGetsController.getUser();
        _meetGetsController.getMeet();
        _taskGetsController.getTask();
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().designWidth(4.14).designHeight(8.96).init(context);
    items = <DashboardItem>[
      DashboardItem(
          title: 'المهام',
          counterAndType: ' المهام ${_countTask.toString()} ',
          icon: Icons.article_rounded,
          iconColor: Color(0xffF26950)),
      DashboardItem(
          title: 'الاجتماعات',
          counterAndType: ' اجتماع ${_countMeet.toString()}',
          icon: Icons.meeting_room_rounded,
          iconColor: Color(0xff30e208)),
      DashboardItem(
          title: 'الموظفين',
          counterAndType: ' موظف ${_countUser.toString()}',
          icon: Icons.person,
          iconColor: Color(0xff6e59e5)),
      DashboardItem(
          title: 'مهمام الاستيراد',
          counterAndType: ' مهمة ${_countImportTask.toString()} ',
          icon: Icons.import_contacts_outlined,
          iconColor: Colors.redAccent),
    ];
    return SafeArea(
      child: Scaffold(
        endDrawerEnableOpenDragGesture: true,
        drawerEdgeDragWidth: 10,
        drawerEnableOpenDragGesture: true,
        extendBody: true,
        extendBodyBehindAppBar: true,
        appBar: AppBar(
          centerTitle: true,
          automaticallyImplyLeading: false,
          elevation: 0,
          backgroundColor: Colors.transparent,
          title: Container(
            margin: EdgeInsetsDirectional.only(
                top: SizeConfig().scaleWidth(30),
                bottom: SizeConfig().scaleWidth(0),
                end: SizeConfig().scaleWidth(0)),
            child: Text(
              'لوحة التحكم',
              style: TextStyle(
                  letterSpacing: SizeConfig().scaleWidth(2),
                  wordSpacing: SizeConfig().scaleWidth(0.5),
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: SizeConfig().scaleWidth(24)),
            ),
          ),
        ),
        body: Stack(
          children: [
            Container(
              width: double.infinity,
              height: double.infinity,
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: AlignmentDirectional.topStart,
                  end: AlignmentDirectional.bottomEnd,
                  colors: [
                    Color(0xffED2424),
                    Colors.white,
                  ],
                ),
              ),
            ),
            SizedBox(
              height: SizeConfig().scaleHeight(140),
            ),
            Align(
              child: Container(
                margin: EdgeInsets.only(top: SizeConfig().scaleHeight(100)),
                width: double.infinity,
                height: double.infinity,
                alignment: Alignment.bottomCenter,
                decoration: BoxDecoration(
                  color: Color(0xffF0F4FD),
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(SizeConfig().scaleWidth(40)),
                    topRight: Radius.circular(SizeConfig().scaleWidth(40)),
                  ),
                ),
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 24),
                  child: GridView.builder(
                      itemCount: items.length,
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 2,
                          mainAxisSpacing: SizeConfig().scaleHeight(24),
                          crossAxisSpacing: SizeConfig().scaleHeight(24),
                          childAspectRatio: SizeConfig().scaleHeight(171) /
                              SizeConfig().scaleWidth(171),
                          mainAxisExtent: SizeConfig().scaleHeight(171)),
                      itemBuilder: (context, index) {
                        return InkWell(
                          onTap: () {
                            if (index == 0) {
                              print('TASK');
                              Navigator.pushNamed(context, 'task_screen');
                            } else if (index == 1) {
                              print('MEET');
                              Navigator.pushNamed(context, 'meet_screen');
                            } else if (index == 2) {
                              Navigator.pushNamed(context, 'employee_screen');
                              print('EMPLOYEE');
                            } else if (index == 3) {
                              Navigator.pushNamed(context, 'import_task');
                              print('IMPORT TASK');
                            } else if (index == 4) {
                              Navigator.pushNamed(context, 'import_task');
                              print('PROFILE');
                            }
                          },
                          child: homeWidget(
                            icon: items[index].icon,
                            counterAndType: items[index].counterAndType,
                            tittle: items[index].title,
                            iconColor: items[index].iconColor,
                          ),
                        );
                      }),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void listValue() {
    DashboardItem(
        title: 'المهام',
        counterAndType: ' المهام ${_countTask.toString()} ',
        icon: Icons.article_rounded,
        iconColor: Color(0xffF26950));
    DashboardItem(
        title: 'الاجتماعات',
        counterAndType: ' اجتماع ${_countMeet.toString()} ',
        icon: Icons.meeting_room_rounded,
        iconColor: Color(0xff30e208));
    DashboardItem(
        title: 'الموظفين',
        counterAndType: ' موظف ${_countUser.toString()}',
        icon: Icons.person,
        iconColor: Color(0xff6e59e5));
    DashboardItem(
        title: 'مهمام الاستيراد',
        counterAndType: ' اجتماع ${_countImportTask.toString()} ',
        icon: Icons.import_contacts_outlined,
        iconColor: Colors.redAccent);
  }

  Future<void> getMeetCount() async {
    int count = await MeetApiController().getMeetCount();
    setState(() {
      _countMeet = count;
    });
  }

  Future<void> getTaskCount() async {
    int count = await TaskApiController().getTaskCount();
    setState(() {
      _countTask = count;
    });
  }

  Future<void> getEmployeeCount() async {
    int count = await UserApiController().getUserCount();
    setState(() {
      _countUser = count;
    });
  }

  Future<void> getImportCount() async {
    int count = await ImportTaskApiController().getImportTaskCount();
    setState(() {
      _countImportTask = count;
    });
  }

  void getItemCount() {
    getTaskCount();
    getMeetCount();
    getEmployeeCount();
    getImportCount();
  }
}
