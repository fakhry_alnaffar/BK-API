import 'package:bio_clean/components/component.dart';
import 'package:bio_clean/getx_controller/meet_getx_controller.dart';
import 'package:bio_clean/getx_controller/task_getx_controller.dart';
import 'package:bio_clean/responsive/size_config.dart';
import 'package:bio_clean/screen/task/edit_task.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class TaskScreen extends StatefulWidget {
  const TaskScreen({Key? key}) : super(key: key);

  @override
  _TaskScreenState createState() => _TaskScreenState();
}

class _TaskScreenState extends State<TaskScreen> {
  TaskGetsController _taskGetsController = Get.put(TaskGetsController());

  @override
  void initState() {
    // TODO: implement initState
    _taskGetsController.getTask();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().designWidth(4.14).designHeight(8.96).init(context);
    return SafeArea(
      child: Scaffold(
        extendBodyBehindAppBar: true,
        appBar: AppBar(
          leading: Container(
              margin: EdgeInsetsDirectional.only(
                  top: SizeConfig().scaleWidth(0),
                  bottom: SizeConfig().scaleWidth(0),
                  start: SizeConfig().scaleWidth(5)),
              child: IconButton(
                icon: Icon(Icons.arrow_back_ios_rounded),
                onPressed: () {
                  Navigator.pushNamed(context, 'home_screen');
                },
              )),          centerTitle: true,
          elevation: 0,
          backgroundColor: Colors.transparent,
          title: Container(
            margin: EdgeInsetsDirectional.only(
                top: SizeConfig().scaleWidth(20),
                bottom: SizeConfig().scaleWidth(0),
                end: SizeConfig().scaleWidth(0)),
            child: Text(
              'المهام',
              style: TextStyle(
                  letterSpacing: 2,
                  wordSpacing: 0.5,
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: SizeConfig().scaleWidth(24)),
            ),
          ),
        ),
        body: Stack(
            children: [
          Container(
            clipBehavior: Clip.antiAlias,
            width: double.infinity,
            height: double.infinity,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: AlignmentDirectional.topStart,
                end: AlignmentDirectional.bottomEnd,
                colors: [
                  Color(0xffED2424),
                  Colors.white,
                ],
              ),
            ),
          ),
          SizedBox(
            height: SizeConfig().scaleHeight(140),
          ),
          Align(
              child: Container(
                clipBehavior: Clip.antiAlias,
                margin: EdgeInsets.only(top: SizeConfig().scaleHeight(100)),
                padding: EdgeInsetsDirectional.only(top: 0),
                width: double.infinity,
                height: double.infinity,
                alignment: Alignment.bottomCenter,
                decoration: BoxDecoration(
                  color: Color(0xffF0F4FD),
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(SizeConfig().scaleWidth(40)),
                    topRight: Radius.circular(SizeConfig().scaleWidth(40)),
                  ),
                ),
                child: GetX<TaskGetsController>(
                    builder: (TaskGetsController controller) {
                      if (controller.tasks.isNotEmpty) {
                        return ListView.separated(
                          physics: BouncingScrollPhysics(),
                          clipBehavior: Clip.antiAlias,
                          shrinkWrap: false,
                          padding: EdgeInsetsDirectional.only(top: 45),
                          itemBuilder: (context, index) {
                            return Padding(
                              padding: const EdgeInsets.symmetric(horizontal: 20),
                              child: InkWell(
                                onTap: (){
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => EditTaskScreen(
                                              task: controller.tasks[index])));
                                },
                                child: showWidgetMeet(
                                    onPressed: () => Navigator.pop(context),
                                    status: 'قيد العمل',
                                    title: controller.tasks[index].name.toString(),
                                    description:
                                    controller.tasks[index].description.toString(),
                                    time: controller.tasks[index].time.toString(),
                                    date: controller.tasks[index].date.toString(),
                                    onDelete: IconButton(
                                      onPressed: () async {
                                        showDialog(
                                            barrierColor:
                                            Colors.black.withOpacity(0.16),
                                            context: context,
                                            builder: (context) {
                                              return AlertDialog(
                                                title: Text('هل انت متاكد ؟'),
                                                actions: [
                                                  TextButton(
                                                      onPressed: () async{
                                                        Navigator.pop(context);
                                                        await deleteTask(index: index);
                                                      },
                                                      child: Text('نعم')),
                                                  TextButton(
                                                      onPressed: () =>
                                                          Navigator.pop(context),
                                                      child: Text('لا')),
                                                ],
// actionsPadding: EdgeInsets.symmetric(horizontal: 50),
                                                shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                  BorderRadius.circular(15),
                                                ),
                                              );
                                            });
                                        print(index);
                                      },
                                      icon: Icon(Icons.delete_rounded,
                                          size: SizeConfig().scaleHeight(26),
                                          color: Colors.red.shade600),
                                    ),
                                ),
                              ),
                            );
                          },
                          separatorBuilder: (BuildContext context, int index) {
                            return SizedBox(
                              height: SizeConfig().scaleHeight(14),
                            );
                          },
                          itemCount: controller.tasks.length,
                        );
                      } else {
                        return Center(
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Icon(
                                Icons.warning,
                                size: 80,
                                color: Colors.grey.shade500,
                              ),
                              Text(
                                'NO Task',
                                style: TextStyle(
                                  color: Colors.grey.shade500,
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ],
                          ),
                        );
                      }
                    }),
              ))
        ]),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            Navigator.pushNamed(context, 'add_task');
          },
          child: Icon(Icons.add),
          backgroundColor: Color(0xffED2424),
        ),
      ),
    );
  }

  Future<void> deleteTask({required int index}) async {
    bool deleted =
        await TaskGetsController.to.deleteTask(context: context, index: index);
  }
}
