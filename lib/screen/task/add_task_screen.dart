import 'package:bio_clean/api/api_model/task_model.dart';
import 'package:bio_clean/api/api_model/user_data_name.dart';
import 'package:bio_clean/getx_controller/task_getx_controller.dart';
import 'package:bio_clean/getx_controller/user_getx_controller.dart';
import 'package:bio_clean/responsive/size_config.dart';
import 'package:bio_clean/utils/helpers.dart';
import 'package:bio_clean/widgets/app_text_filed.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class AddTask extends StatefulWidget {
  @override
  _AddMeetState createState() => _AddMeetState();
}

class _AddMeetState extends State<AddTask> with Helpers {
  late var _meetNameController;
  late var _meetTimeController;
  late var _meetDateController;
  late var _meetDescriptionController;
  late Color borderColor = Color(0x595a55ca);
  List<Users> teem = <Users>[];
  List<Users> teemSelected = <Users>[];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _meetNameController = TextEditingController();
    _meetTimeController = TextEditingController();
    _meetDateController = TextEditingController();
    _meetDescriptionController = TextEditingController();
    getCustomerName();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _meetNameController.dispose();
    _meetTimeController.dispose();
    _meetDateController.dispose();
    _meetDescriptionController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().designWidth(4.14).designHeight(8.96).init(context);
    return SafeArea(
      child: Scaffold(
        extendBodyBehindAppBar: true,
        appBar: AppBar(
          centerTitle: true,
          elevation: 0,
          automaticallyImplyLeading: true,
          backgroundColor: Colors.transparent,
          title: Container(
            margin: EdgeInsetsDirectional.only(
                top: SizeConfig().scaleWidth(20),
                bottom: SizeConfig().scaleWidth(10),
                end: SizeConfig().scaleWidth(0)),
            child: Text(
              'اضافة مهمة',
              style: TextStyle(
                  letterSpacing: 2,
                  wordSpacing: 0.5,
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: SizeConfig().scaleWidth(24)),
            ),
          ),
        ),
        body: Stack(
          children: [
            Container(
              width: double.infinity,
              height: double.infinity,
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: AlignmentDirectional.topStart,
                  end: AlignmentDirectional.bottomEnd,
                  colors: [
                    Color(0xffED2424),
                    Colors.white,
                  ],
                ),
              ),
            ),
            SizedBox(
              height: SizeConfig().scaleHeight(140),
            ),
            Align(
              child: Container(
                margin: EdgeInsets.only(top: SizeConfig().scaleHeight(100)),
                width: 414,
                height: double.infinity,
                alignment: Alignment.bottomCenter,
                decoration: BoxDecoration(
                  color: Color(0xffF0F4FD),
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(40),
                    topRight: Radius.circular(40),
                  ),
                ),
                child: Padding(
                  padding: EdgeInsets.all(SizeConfig().scaleWidth(24)),
                  child: Align(
                    alignment: Alignment.topCenter,
                    child: SingleChildScrollView(
                      physics: BouncingScrollPhysics(),
                      child: Column(
                        children: [
                          SizedBox(
                            height: SizeConfig().scaleHeight(40),
                          ),
                          AppTextFiled(
                            labelText: 'اسم المهمة',
                            controller: _meetNameController,
                            prefix: Icons.title,
                          ),
                          SizedBox(
                            height: SizeConfig().scaleHeight(20),
                          ),
                          AppTextFiled(
                            textInputType: TextInputType.datetime,
                            labelText: 'وقت المهمة',
                            // isEnabled: false,
                            controller: _meetTimeController,
                            prefix: Icons.timer,
                            onTap: () {
                              showTimePicker(
                                context: context,
                                initialTime: TimeOfDay.now(),
                              ).then((value) => _meetTimeController.text =
                                  value!.format(context).toString());
                            },
                          ),
                          SizedBox(
                            height: SizeConfig().scaleHeight(20),
                          ),
                          AppTextFiled(
                            textInputType: TextInputType.datetime,
                            labelText: 'تاريخ المهمة',
                            // isEnabled: false,
                            controller: _meetDateController,
                            prefix: Icons.date_range_rounded,
                            onTap: () {
                              showDatePicker(
                                context: context,
                                initialDate: DateTime.now(),
                                firstDate: DateTime.now(),
                                lastDate:
                                    DateTime.now().add(Duration(days: 60)),
                              ).then((value) {
                                // dateController.text=value.toString();
                                //باقي الشغل
                                _meetDateController.text = DateFormat.yMMMd()
                                    .format(value!)
                                    .toString();
                                // print(DateFormat.yMMMd().format(value!));
                              });
                            },
                          ),
                          SizedBox(
                            height: SizeConfig().scaleHeight(20),
                          ),
                          AppTextFiled(
                            labelText: 'وصف المهمة',
                            controller: _meetDescriptionController,
                            prefix: Icons.description_rounded,
                          ),
                          SizedBox(
                            height: SizeConfig().scaleHeight(20),
                          ),
                          // InkWell(
                          //     onTap: () {
                          //       showBottomSheet();
                          //     },
                          //     child: Container(
                          //       width: double.infinity,
                          //       height: 80,
                          //       padding: EdgeInsets.symmetric(horizontal: 0),
                          //       decoration: BoxDecoration(
                          //           border: Border.all(
                          //               color: Color(0xff5A55CA), width: 2),
                          //           color: Color(0xffED2424),
                          //           borderRadius: BorderRadius.only(
                          //             topRight: Radius.circular(10.0),
                          //             bottomLeft: Radius.circular(10.0),
                          //             bottomRight: Radius.circular(10.0),
                          //             topLeft: Radius.circular(10.0),
                          //           )),
                          //       child: teemSelected.length < 0
                          //           ? Text('data')
                          //           : Container(
                          //               margin: EdgeInsetsDirectional.only(
                          //                   start: 0, end: 0),
                          //               child: SingleChildScrollView(
                          //                 scrollDirection: Axis.horizontal,
                          //                 physics: BouncingScrollPhysics(),
                          //                 clipBehavior:
                          //                     Clip.antiAliasWithSaveLayer,
                          //                 padding: EdgeInsets.zero,
                          //                 child: Row(children: [
                          //                   Container(
                          //                     margin:
                          //                         EdgeInsetsDirectional.only(
                          //                             start: 10, end: 10),
                          //                     child: Row(
                          //                       children: [
                          //                         Icon(
                          //                           Icons.shop_outlined,
                          //                           color: Color(0xffED2424),
                          //                           size: 25,
                          //                         ),
                          //                         SizedBox(
                          //                           width: SizeConfig()
                          //                               .scaleWidth(10),
                          //                         ),
                          //                         Text(
                          //                           ' اسم المحل',
                          //                           style: TextStyle(
                          //                               color:
                          //                                   Color(0xffED2424),
                          //                               fontSize: 16),
                          //                         ),
                          //                         ListView.separated(
                          //                           padding:
                          //                               EdgeInsetsDirectional
                          //                                   .zero,
                          //                           physics:
                          //                               BouncingScrollPhysics(),
                          //                           shrinkWrap: true,
                          //                           itemCount:
                          //                               teemSelected.length,
                          //                           scrollDirection:
                          //                               Axis.horizontal,
                          //                           itemBuilder:
                          //                               (context, index) {
                          //                             return Column(
                          //                               mainAxisSize:
                          //                                   MainAxisSize.max,
                          //                               mainAxisAlignment:
                          //                                   MainAxisAlignment
                          //                                       .center,
                          //                               crossAxisAlignment:
                          //                                   CrossAxisAlignment
                          //                                       .center,
                          //                               children: [
                          //                                 CircleAvatar(
                          //                                   child: Icon(
                          //                                     Icons
                          //                                         .shop_outlined,
                          //                                     color:
                          //                                         Colors.white,
                          //                                     size: 20,
                          //                                   ),
                          //                                   backgroundColor:
                          //                                       Color(
                          //                                           0xffED2424),
                          //                                   radius: 20,
                          //                                 ),
                          //                                 SizedBox(
                          //                                   height: 5,
                          //                                 ),
                          //                                 Text(
                          //                                   //here work to show shop name
                          //                                   teemSelected[index].name,
                          //                                   style: TextStyle(
                          //                                       color: Colors
                          //                                           .black,
                          //                                       fontWeight:
                          //                                           FontWeight
                          //                                               .w500,
                          //                                       fontSize: 16),
                          //                                 ),
                          //                               ],
                          //                             );
                          //                           },
                          //                           separatorBuilder:
                          //                               (BuildContext context,
                          //                                   int index) {
                          //                             return SizedBox(
                          //                               width: SizeConfig()
                          //                                   .scaleWidth(10),
                          //                             );
                          //                           },
                          //                         ),
                          //                       ],
                          //                     ),
                          //                   ),
                          //                 ]),
                          //               ),
                          //             ),
                          //     )),
                          // SizedBox(
                          //   height: SizeConfig().scaleHeight(30),
                          // ),
                          ElevatedButton(
                            style: ElevatedButton.styleFrom(
                              minimumSize: Size(double.infinity,
                                  SizeConfig().scaleHeight(60)),
                              primary: Color(0xffED2424),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10),
                              ),
                            ),
                            onPressed: () async {
                              // Navigator.pushReplacementNamed(
                              //     context, '/main_screen');
                              await perfCreateNewMeet();
                            },
                            child: Text(
                              'تاكيد العملية',
                              style: TextStyle(
                                  fontSize: SizeConfig().scaleTextFont(20)),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void showBottomSheet() {
    showModalBottomSheet(
        barrierColor: Colors.black.withOpacity(0.25),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15),
        ),
        context: context,
        builder: (context) {
          return Padding(
              padding: EdgeInsets.symmetric(vertical: 15),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Expanded(
                    child: StatefulBuilder(
                      builder: (BuildContext context,
                          void Function(void Function()) setState) {
                        return ListView.builder(
                          itemCount: teem.length,
                          itemBuilder: (context, index) {
                            return CheckboxListTile(
                              title: Text(teem[index].name.toString()),
                              value: teem[index].states,
                              onChanged: (status) {
                                setState(() {
                                  teem[index].states = status!;
                                });
                                print(teem[index].name);
                              },
                            );
                          },
                        );
                      },
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(16),
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        minimumSize:
                            Size(double.infinity, SizeConfig().scaleHeight(60)),
                        primary: Color(0xffED2424),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                      ),
                      onPressed: () {
                        setState(() {
                          teemSelected.clear();
                          for (Users teem in teem) {
                            if (teem.states) {
                              teemSelected.add(teem);
                              print('on pressed functiton teem  $teem');
                            }
                          }
                        });
                        Navigator.pop(context);
                      },
                      child: Text(
                        'حسنا',
                        style:
                            TextStyle(fontSize: SizeConfig().scaleTextFont(20)),
                      ),
                    ),
                  ),
                ],
              ));
        });
  }

  Future<void> perfCreateNewMeet() async {
    if (checkData()) {
      await crateTask();
      // print('wdhau');
    }
  }

  bool checkData() {
    if (_meetNameController.text.isNotEmpty &&
        _meetTimeController.text.isNotEmpty &&
        _meetDateController.text.isNotEmpty &&
        _meetDescriptionController.text.isNotEmpty) {
      return true;
    }
    showSnackBar(context: context, message: 'Enter Required Data', error: true);
    return false;
  }

  Future<void> crateTask() async {
    bool crated =
        await TaskGetsController.to.crateTask(context: context, task: task);
    if (crated) {
      clear();
      showSnackBar(context: context, message: 'TASK ADDED SUCCESSFULLY');
      Navigator.pop(context);
    }
    // Navigator.pushReplacementNamed(context, 'main_screen');
  }

  Task get task {
    Task task = Task();
    task.name = _meetNameController.text;
    task.description = _meetDescriptionController.text;
    task.time = _meetTimeController.text;
    task.date = _meetDateController.text;
print(_meetNameController.text);
print(_meetDescriptionController.text);
print(_meetTimeController.text);
print(_meetDateController.text);
    return task;
  }

  void clear() {
    _meetNameController.text = '';
    _meetDescriptionController.text = '';
    _meetTimeController.text = '';
    _meetDateController.text = '';
  }
  //
  Future<void> getCustomerName() async {
    List<String> names = await UserGetsController.to.getUserNameData();
    for (int i = 0; i < names.length; i++) {
      setState(() {
        teem.add(Users(name: names[i].toString(), states: false));
      });
    }
  }
}
