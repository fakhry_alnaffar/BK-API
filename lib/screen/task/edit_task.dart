import 'package:bio_clean/api/api_model/meet_model.dart';
import 'package:bio_clean/api/api_model/task_model.dart';
import 'package:bio_clean/getx_controller/meet_getx_controller.dart';
import 'package:bio_clean/getx_controller/task_getx_controller.dart';
import 'package:bio_clean/responsive/size_config.dart';
import 'package:bio_clean/utils/helpers.dart';
import 'package:bio_clean/widgets/app_text_filed.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class EditTaskScreen extends StatefulWidget {
  late final Task task;

  EditTaskScreen({required this.task});
  @override
  _AddMeetState createState() => _AddMeetState();
}

class _AddMeetState extends State<EditTaskScreen> with Helpers {
  late TextEditingController _name = TextEditingController();
  late TextEditingController _description = TextEditingController();
  late TextEditingController _time = TextEditingController();
  late TextEditingController _date = TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    _name = TextEditingController(text: widget.task.name);
    _description = TextEditingController(text: widget.task.description);
    _time = TextEditingController(text: widget.task.time.toString());
    _date = TextEditingController(text: widget.task.date.toString());
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _name.dispose();
    _description.dispose();
    _time.dispose();
    _date.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().designWidth(4.14).designHeight(8.96).init(context);
    return SafeArea(
      child: Scaffold(
        extendBodyBehindAppBar: true,
        appBar: AppBar(
          centerTitle: true,
          elevation: 0,
          leading: Container(
              margin: EdgeInsetsDirectional.only(
                  top: SizeConfig().scaleWidth(20),
                  bottom: SizeConfig().scaleWidth(10),
                  start: SizeConfig().scaleWidth(5)),
              child: IconButton(
                icon: Icon(Icons.arrow_back_ios_rounded),
                onPressed: () {
                  Navigator.pop(context);
                },
              )),
          backgroundColor: Colors.transparent,
          title: Container(
            margin: EdgeInsetsDirectional.only(
                top: SizeConfig().scaleWidth(20),
                bottom: SizeConfig().scaleWidth(10),
                end: SizeConfig().scaleWidth(0)),
            child: Text(
              'تعديل المهمة',
              style: TextStyle(
                  letterSpacing: SizeConfig().scaleWidth(2),
                  wordSpacing: SizeConfig().scaleWidth(0.5),
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: SizeConfig().scaleWidth(24)),
            ),
          ),
        ),
        body: Stack(
          children: [
            Container(
              width: double.infinity,
              height: double.infinity,
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: AlignmentDirectional.topStart,
                  end: AlignmentDirectional.bottomEnd,
                  colors: [
                    Color(0xffED2424),
                    Colors.white,
                  ],
                ),
              ),
            ),
            SizedBox(
              height: SizeConfig().scaleHeight(140),
            ),
            Align(
              child: Container(
                margin: EdgeInsets.only(top: SizeConfig().scaleHeight(100)),
                width: double.infinity,
                height: double.infinity,
                alignment: Alignment.bottomCenter,
                decoration: BoxDecoration(
                  color: Color(0xffF0F4FD),
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(SizeConfig().scaleWidth(40)),
                    topRight: Radius.circular(SizeConfig().scaleWidth(40)),
                  ),
                ),
                child: Padding(
                  padding: EdgeInsets.all(SizeConfig().scaleWidth(24)),
                  child: Align(
                    alignment: Alignment.topCenter,
                    child: SingleChildScrollView(
                      physics: BouncingScrollPhysics(),
                      child: Column(
                        children: [
                          SizedBox(
                            height: SizeConfig().scaleHeight(40),
                          ),
                          AppTextFiled(
                            labelText: 'اسم المهمة',
                            controller: _name,
                            prefix: Icons.title,
                          ),
                          SizedBox(
                            height: SizeConfig().scaleHeight(20),
                          ),
                          AppTextFiled(
                            validator: (String? value) {
                              if (value == null) {
                                return 'S';
                              }
                              return null;
                            },
                            textInputType: TextInputType.datetime,
                            labelText: 'وقت المهمة',
                            // isEnabled: false,
                            controller: _time,
                            prefix: Icons.timer,
                            onTap: () {
                              showTimePicker(
                                context: context,
                                initialTime: TimeOfDay.now(),
                              )
                                  .then((value) => _time.text =
                                      value!.format(context).toString())
                                  .catchError((error) => null);
                            },
                          ),
                          SizedBox(
                            height: SizeConfig().scaleHeight(20),
                          ),
                          AppTextFiled(
                            textInputType: TextInputType.datetime,
                            labelText: 'تاريخ المهمة',
                            // isEnabled: false,
                            controller: _date,
                            prefix: Icons.date_range_rounded,
                            onTap: () {
                              showDatePicker(
                                context: context,
                                initialDate: DateTime.now(),
                                firstDate: DateTime.now(),
                                lastDate:
                                    DateTime.now().add(Duration(days: 60)),
                              ).then((value) {
                                // dateController.text=value.toString();
                                //باقي الشغل
                                _date.text = DateFormat.yMMMd()
                                    .format(value!)
                                    .toString();
                                // print(DateFormat.yMMMd().format(value!));
                              });
                            },
                          ),
                          SizedBox(
                            height: SizeConfig().scaleHeight(20),
                          ),
                          AppTextFiled(
                            labelText: 'وصف المهمة',
                            controller: _description,
                            prefix: Icons.description_rounded,
                          ),
                          SizedBox(
                            height: SizeConfig().scaleHeight(30),
                          ),
                          ElevatedButton(
                            style: ElevatedButton.styleFrom(
                              minimumSize: Size(double.infinity,
                                  SizeConfig().scaleHeight(60)),
                              primary: Color(0xffED2424),

                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10),
                              ),
                            ),
                            onPressed: () async{
                              await perfUpdateTask();
                            },
                            child: Text(
                              'تاكيد العملية',
                              style: TextStyle(
                                  fontSize: SizeConfig().scaleTextFont(20)),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<void> perfUpdateTask() async {
    if (checkData()) {
      // await updateTask();
    } else {}
  }

  bool checkData() {
    if (_name.text.isNotEmpty&&_time.text.isNotEmpty&&_date.text.isNotEmpty&&_description.text.isNotEmpty) {
      return true;
    }
    showSnackBar(context: context, message: 'Enter Required Data', error: true);
    return false;
  }

  // Future<void> updateTask() async {
  //   bool updated =
  //   await TaskGetsController.to.updateTask(context: context, task: task);
  //   if (updated) {
  //     Navigator.pop(context);
  //   }
  // }
  //
  //
  // Task get task {
  //   Task task = widget.task;
  //   task.id = widget.task.id;
  //   task.name = _name.text;
  //   task.description = _description.text;
  //   task.time = _time.text;
  //   task.date = _date.text;
  //   return task;
  // }

}
//0xff5A55CA
