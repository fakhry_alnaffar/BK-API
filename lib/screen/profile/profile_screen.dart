import 'package:bio_clean/components/component.dart';
import 'package:bio_clean/responsive/size_config.dart';
import 'package:flutter/material.dart';

import 'edit_profile.dart';

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({Key? key}) : super(key: key);

  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  String name = '';
  @override
  void initState() {
    // TODO: implement initState
    getUserName();
    setState(() {
      getUserName();
    });
    super.initState();
    getUserName();
    setState(() {
      getUserName();
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    getUserName();
    setState(() {
      getUserName();
    });
    super.dispose();
    getUserName();
    setState(() {
      getUserName();
    });
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().designWidth(4.14).designHeight(8.96).init(context);
    return Scaffold(
      endDrawerEnableOpenDragGesture: true,
      drawerEdgeDragWidth: 10,
      drawerEnableOpenDragGesture: true,
      extendBody: true,
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        centerTitle: true,
        elevation: 0,
        backgroundColor: Colors.transparent,
        title: Container(
          margin: EdgeInsetsDirectional.only(
              top: SizeConfig().scaleWidth(5),
              bottom: SizeConfig().scaleWidth(0),
              end: SizeConfig().scaleWidth(0)),
          child: Text(
            'بيانات الحساب',
            style: TextStyle(
                letterSpacing: 2,
                wordSpacing: 0.5,
                color: Colors.white,
                fontWeight: FontWeight.bold,
                fontSize: SizeConfig().scaleWidth(24)),
          ),
        ),
      ),
      body: Stack(
        children: [
          Container(
            width: double.infinity,
            height: double.infinity,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: AlignmentDirectional.topStart,
                end: AlignmentDirectional.bottomEnd,
                colors: [
                  Color(0xff5A55CA),
                  Colors.white,
                ],
              ),
            ),
          ),
          SizedBox(
            height: SizeConfig().scaleHeight(140),
          ),
          Align(
            child: Container(
              margin: EdgeInsets.only(top: 100),
              width: 414,
              height: double.infinity,
              alignment: Alignment.bottomCenter,
              decoration: BoxDecoration(
                color: Color(0xffF0F4FD),
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(40),
                  topRight: Radius.circular(40),
                ),
              ),
              child: Column(
                children: [
                  SizedBox(height: 40),
                  DecoratedBox(
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      boxShadow: [
                        BoxShadow(
                          offset: Offset(0, 0),
                          color: Colors.white.withOpacity(0.16),
                          blurRadius: 6,
                        ),
                      ],
                    ),
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(80),
                        border: Border.all(width: 8, color: Colors.white),
                      ),
                      child: CircleAvatar(
                        maxRadius: 50,
                        minRadius: 50,
                        backgroundColor: Color(0xff5A55CA),
                        backgroundImage: NetworkImage(
                            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRPtENZS1gdjgiNbiX6gLEZPDCiKUXSkSo9SY4ZnWPxGwESsCxZeoXUOIQiFsD8ph-WmAc&usqp=CAU'),
                      ),
                    ),
                  ),
                  SizedBox(height: 14),
                  Text(
                    'الاسم',
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                      color: Color(0xff0B204C),
                    ),
                  ),
                  SizedBox(height: 5),
                  Text(
                    'المسمى الوظيفي',
                    style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.bold,
                      color: Color(0xffb2bac9),
                    ),
                  ),
                  SizedBox(height: 30),
                  Column(
                    children: [
                      Row(
                        children: [
                          SizedBox(
                            width: 25,
                          ),
                          Icon(
                            Icons.settings,
                            color: Color(0xff5A55CA),
                            size: 25,
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          Text(
                            ' الاعددادت ',
                            style: TextStyle(
                              fontSize: 17,
                              color: Color(0xff0B204C),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      InkWell(
                        child: showItemProfile(
                            title: 'تعديل بيانات الحساب',
                            subtitle: 'يمكنك تعديل بياناتك',
                            icon: Icons.edit_outlined),
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => EditProfile()),
                          );
                        },
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      InkWell(
                        child: showItemProfile(
                            title: 'تغير كلمة المرور',
                            subtitle: 'يمكنك تعديل كلمة المرور',
                            icon: Icons.lock_outline_rounded),
                        onTap: () {
                          Navigator.pushNamed(context, 'change_password');
                        },
                      ),
                    ],
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Future<void> getUserName() async {
    String namee ='name';
    setState(() {
      name = namee;
      print('Name ' + name);
    });
  }
}
