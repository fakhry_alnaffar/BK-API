import 'package:flutter/material.dart';

class BnScreen{
  late final String title;
  late final Widget widget;

  BnScreen({
   required this.title,
   required this.widget
  });
}