class StudentImage{
  late String image;
  late int studentId;
  late int id;
  StudentImage.fromJson(Map<String, dynamic> jsonObject){
    image = jsonObject['image'];
    studentId = jsonObject['student_id'];
    id = jsonObject['id'];
  }
}