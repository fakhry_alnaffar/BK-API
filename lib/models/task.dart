class Task {
  late int id;
  late String title;
  late int studentId;
  late String createAt;
  late String updateAt;
  late bool isDone;

  Task();

  Task.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    studentId = json['student_id'];
    createAt = json['created_at'];
    updateAt = json['updated_at'];
    isDone = json['is_done'];
  }
}
