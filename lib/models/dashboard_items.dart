import 'package:flutter/cupertino.dart';

class DashboardItem{
 late String title;
 late String counterAndType;
 late var icon;
 late Color iconColor;

 DashboardItem({
 required this.title,
 required this.counterAndType,
 required this.icon,
 required this.iconColor
 });
}