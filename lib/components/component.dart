import 'package:bio_clean/responsive/size_config.dart';
import 'package:flutter/material.dart';

Widget homeWidget(
    {required var icon,
    required String tittle,
    required String counterAndType,
    required Color iconColor}) {
  return Stack(
    children: [
      Container(
        width: SizeConfig().scaleWidth(171),
        height: SizeConfig().scaleHeight(171),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(14),
          boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(0.16),
              spreadRadius: 0,
              blurRadius: 4,
              offset: Offset(0, 4), // changes position of shadow
            ),
          ],
        ),
      ),
      Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Container(
              height: SizeConfig().scaleHeight(59),
              width: SizeConfig().scaleWidth(59),
              child: Icon(
                icon,
                color: iconColor,
                size: SizeConfig().scaleWidth(40),
              ),
              decoration: BoxDecoration(
                color: Color(0xFFF0F4FD),
                borderRadius: BorderRadius.circular(14),
              ),
            ),
            SizedBox(
              height: SizeConfig().scaleHeight(16),
            ),
            Text(
              tittle,
              style: TextStyle(
                fontSize: SizeConfig().scaleTextFont(16),
                fontWeight: FontWeight.w500,
                color: Color(0xff0B204C),
              ),
            ),
            SizedBox(
              height: SizeConfig().scaleHeight(8),
            ),
            Text(
              '$counterAndType',
              style: TextStyle(
                  fontSize: SizeConfig().scaleTextFont(16),
                  fontWeight: FontWeight.w500,
                  color: Color(0xFFB2BAC9)),
            ),
          ],
        ),
      )
    ],
  );
}

Widget showWidgetMeet(
    {required String status,
    required String title,
    required String description,
    required String time,
    required Widget onDelete,
    Function? onPressed,
    required String date}) {
  return Container(
    clipBehavior: Clip.antiAlias,
    margin: EdgeInsetsDirectional.only(bottom: 20),
    decoration: BoxDecoration(
      color: Colors.white,
      borderRadius: BorderRadius.circular(15),

    ),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: EdgeInsets.only(
              top: SizeConfig().scaleHeight(15),
              left: SizeConfig().scaleWidth(15)),
          child: Container(
            margin: EdgeInsetsDirectional.only(start: 25),
            alignment: AlignmentDirectional.topStart,
            child: Text(
              status,
              maxLines: 1,
              overflow: TextOverflow
                  .ellipsis,
              textAlign: TextAlign.start,
              style: TextStyle(
                  fontSize: SizeConfig().scaleTextFont(16),
                  color: Color(0xff6e59e5)),
            ),
          ),
        ),

        Divider(
          indent: SizeConfig().scaleWidth(15),
          endIndent: SizeConfig().scaleWidth(15),
          color: Colors.grey,
        ),
        SizedBox(
          height: SizeConfig().scaleWidth(10),
        ),
        Row(
          children: [
            SizedBox(
              width: SizeConfig().scaleWidth(15),
            ),
            Container(
              width: SizeConfig().scaleWidth(3),
              height: SizeConfig().scaleHeight(50),
              color: Color(0xff6e59e5),
            ),
            SizedBox(
              width: SizeConfig().scaleWidth(15),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  title,
                  maxLines: 1,
                  overflow: TextOverflow
                      .ellipsis,
                  style: TextStyle(
                    fontSize: SizeConfig().scaleTextFont(18),
                    fontWeight: FontWeight.bold,
                    color: Color(0xff0B204C),
                  ),
                ),
                SizedBox(
                  height: SizeConfig().scaleHeight(5),
                ),
                SizedBox(
                  width: 260,
                  child: Text(
                    description,
                    maxLines: 1,
                    overflow: TextOverflow
                        .ellipsis,
                    style: TextStyle(
                        fontSize: SizeConfig().scaleTextFont(15),
                        color: Color(0xffB2BAC9)),
                  ),
                ),
              ],
            ),
            Spacer(),
            SizedBox(
                width: 35,child: onDelete),
            SizedBox(
              width: SizeConfig().scaleWidth(10),
            ),
          ],
        ),
        SizedBox(
          height: SizeConfig().scaleHeight(15),
        ),
        Row(
          children: [
            SizedBox(
              width: SizeConfig().scaleWidth(15),
            ),
            Row(
              children: [
                Icon(
                  Icons.timer,
                  color: Colors.grey,
                  size: SizeConfig().scaleHeight(20),
                ),
                SizedBox(
                  width: SizeConfig().scaleHeight(10),
                ),
                Text(
                  time,
                  style: TextStyle(
                      color: Color(0xff0B204C),
                      fontSize: SizeConfig().scaleTextFont(14)),
                )
              ],
            ),
            Spacer(),
            Row(
              children: [
                Icon(
                  Icons.date_range_rounded,
                  color: Colors.grey,
                  size: SizeConfig().scaleHeight(20),
                ),
                SizedBox(
                  width: SizeConfig().scaleWidth(10),
                ),
                Text(
                  date,
                  style: TextStyle(
                      color: Color(0xff0B204C),
                      fontSize: SizeConfig().scaleTextFont(14)),
                ),
                SizedBox(
                  width: SizeConfig().scaleWidth(15),
                ),
              ],
            ),
          ],
        ),
        SizedBox(
          height: SizeConfig().scaleHeight(15),
        ),
      ],
    ),
  );
}

Widget showEmployee({
  required BuildContext context,
  required String title,
  required String subtitle,
  required String image,
  required Widget widgetDelete
}) {
  return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(15),
      ),
      alignment: AlignmentDirectional.center,
      child: Padding(
        padding: const EdgeInsets.only(top: 3.0, bottom: 3),
        child: Container(
          height: SizeConfig().scaleHeight(90),
          width: SizeConfig().scaleWidth(366),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(14),
          ),
          child: Row(
            children: [
              widgetDelete,
              Spacer(),
              Column(
                crossAxisAlignment: CrossAxisAlignment.baseline,
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                textBaseline: TextBaseline.ideographic,
                children: [

                  Text(
                    title,
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    textAlign: TextAlign.end,

                    softWrap: false,
                    style: TextStyle(
                        color: Color(0xff0B204C),
                        fontWeight: FontWeight.w400,
                        fontSize: 16,
                        textBaseline: TextBaseline.ideographic),
                  ),
                  SizedBox(
                    height: 3,
                  ),
                  Text(
                    subtitle,
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    textAlign: TextAlign.end,
                    style: TextStyle(
                        color: Color(0xff919BB3),
                        fontSize: 13,
                        textBaseline: TextBaseline.ideographic),
                  ),
                ],
              ),
              SizedBox(width: 15,),

              Padding(
                padding: const EdgeInsets.only(top: 16, bottom: 16, left: 0),
                child: CircleAvatar(
                  radius: 45,
                  backgroundImage: NetworkImage(image),
                  backgroundColor: Color(0xff5A55CA),
                ),
              ),
              SizedBox(
                width: 5,
              ),
            ],
          ),
        ),
      ));
}
Widget showItemProfile({
  required String title,
  required String subtitle,
  required IconData icon,
}) {
  return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(15),
      ),
      child: Padding(
        padding: const EdgeInsets.only(top: 3.0, bottom: 3),
        child: Container(
          height: SizeConfig().scaleHeight(90),
          width: SizeConfig().scaleWidth(366),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(14),
          ),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            mainAxisSize: MainAxisSize.max,
            textBaseline: TextBaseline.ideographic,
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 16, bottom: 16, left: 0),
                child: CircleAvatar(
                  radius: 45,
                  child: Icon(
                    icon,
                    color: Colors.white,
                  ),
                  backgroundColor: Color(0xff5A55CA),
                ),
              ),
              SizedBox(
                width: 0,
              ),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.baseline,
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  textBaseline: TextBaseline.ideographic,
                  children: [
                    Text(
                      title,
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      softWrap: false,
                      textAlign: TextAlign.start,
                      style: TextStyle(
                          color: Color(0xff0B204C),
                          fontWeight: FontWeight.w400,
                          fontSize: 16,
                          textBaseline: TextBaseline.ideographic),
                    ),
                    SizedBox(
                      height: 3,
                    ),
                    Text(
                      subtitle,
                      textAlign: TextAlign.start,
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                          color: Color(0xff919BB3),
                          fontSize: 13,
                          textBaseline: TextBaseline.ideographic),
                    ),
                  ],
                ),
              ),
              SizedBox(
                width: 30,
              ),

            ],
          ),
        ),
      ));
}