import 'package:bio_clean/api/api_controller/meet_api_controller.dart';
import 'package:bio_clean/api/api_controller/task_api_controller.dart';
import 'package:bio_clean/api/api_controller/user_api_controller.dart';
import 'package:bio_clean/api/api_model/meet_model.dart';
import 'package:bio_clean/api/api_model/user_model.dart';
import 'package:bio_clean/utils/helpers.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class UserGetsController extends GetxController with Helpers {
  RxList<UserData> users = <UserData>[].obs;
  UserApiController _userApiController = UserApiController();
  static UserGetsController get to=>Get.find();

  @override
  void onInit() {
    // TODO: implement onInit
    getUser();
    super.onInit();
  }

  Future<void> getUser() async {
    users.value = await _userApiController.getUser();
  }


  Future<List<String>> getUserNameData() async {
    return await _userApiController.getUser().then((value) {
      List<String> names = <String>[];
      for (int i = 0; i < value.length; i++) {
        names.add(value[i].name.toString());
      }
      return names;
    });
  }

  Future<bool> crateUser(
      {required BuildContext context, required UserData user}) async {
    UserData? newTask =
    await _userApiController.createUser(context: context, user: user);
    if (newTask != null) {
      users.add(newTask);
      return true;
    }

    return newTask != null;
  }

  Future<bool> updateUser(
      {required BuildContext context,
        required UserData user}) async {
    bool updated =
    await _userApiController.updateUser(context: context, user: user);
    if (updated) {
      int index = users.indexWhere((element) => element.id == user.id);
      if (index != -1) {
        users[index] = user;
      }
    }
    return updated;
  }

  Future<bool> deleteUser(
      {required BuildContext context, required int index}) async {
    bool deleted = await _userApiController.deleteUser(
        context: context, id: users[index].id);
    if (deleted) {
      users.removeAt(index);
      showSnackBar(context: context, message: 'تم حذف الموظف');
    }else{
      showSnackBar(context: context, message: 'تعذرت عملية حذف الموظف',error: true);
    }
    return deleted;
  }
}
