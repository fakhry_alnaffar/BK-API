import 'package:bio_clean/api/api_controller/meet_api_controller.dart';
import 'package:bio_clean/api/api_controller/task_api_controller.dart';
import 'package:bio_clean/api/api_model/meet_model.dart';
import 'package:bio_clean/api/api_model/task_model.dart';
import 'package:bio_clean/utils/helpers.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class TaskGetsController extends GetxController with Helpers{
  RxList<Task> tasks = <Task>[].obs;
  TaskApiController _taskApiController = TaskApiController();
  static TaskGetsController get to=>Get.find();

  @override
  void onInit() {
    // TODO: implement onInit
    getTask();
    super.onInit();
  }

  Future<void> getTask() async {
    tasks.value = await _taskApiController.getTask();
  }

  Future<bool> crateTask(
      {required BuildContext context, required Task task}) async {
    Task? newTask =
    await _taskApiController.createTask(context: context, task: task);
    if (newTask != null) {
      tasks.add(newTask);
      return true;
    }

    return newTask != null;
  }

  Future<bool> updateTask(
      {required BuildContext context,
        required Task task}) async {
    bool updated =
    await _taskApiController.updateTask(context: context, task: task);
    if (updated) {
      int index = tasks.indexWhere((element) => element.id == task.id);
      if (index != -1) {
        tasks[index] = task;
      }
    }
    return updated;
  }

  Future<bool> deleteTask(
      {required BuildContext context, required int index}) async {
    bool deleted = await _taskApiController.deleteTask(
        context: context, id: tasks[index].id);
    if (deleted) {
      tasks.removeAt(index);
      showSnackBar(context: context, message: 'تم حذف المهمة');
    }else{
      showSnackBar(context: context, message: 'تعذرت عملية حذف المهمة',error: true);
    }
    return deleted;
  }
}
