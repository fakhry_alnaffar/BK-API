import 'package:bio_clean/api/api_controller/meet_api_controller.dart';
import 'package:bio_clean/api/api_model/meet_model.dart';
import 'package:bio_clean/utils/helpers.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class MeetGetsController extends GetxController with Helpers {
  RxList<Meet> meets = <Meet>[].obs;
  MeetApiController _meetApiController = MeetApiController();
  static MeetGetsController get to=>Get.find();

  @override
  void onInit() {
    // TODO: implement onInit
    getMeet();
    super.onInit();
  }

  Future<void> getMeet() async {
    meets.value = await _meetApiController.getMeet();
  }

  Future<bool> crateMeet(
      {required BuildContext context, required Meet meet}) async {
    Meet? newMeet =
    await _meetApiController.createMeet(context: context, meet: meet);
    if (newMeet != null) {
      meets.add(newMeet);
      return true;
    }

    return newMeet != null;
  }

  Future<bool> updateMeet(
      {required BuildContext context,
        required Meet meet}) async {
    bool updated =
    await _meetApiController.updateMeet(context: context, meet: meet);
    if (updated) {
      int index = meets.indexWhere((element) => element.id == meet.id);
      if (index != -1) {
        meets[index] = meet;
      }
    }
    return updated;
  }

  Future<bool> deleteMeet(
      {required BuildContext context, required int index}) async {
    bool deleted = await _meetApiController.deleteMeet(
        context: context, id: meets[index].id);
    if (deleted) {
      meets.removeAt(index);
      showSnackBar(context: context, message: 'تم حذف الاجتماع');
    }else{
      showSnackBar(context: context, message: 'تعذرت عملية حذف الاجتماع',error: true);
    }
    return deleted;
  }
}
