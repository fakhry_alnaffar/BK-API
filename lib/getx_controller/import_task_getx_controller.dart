import 'package:bio_clean/api/api_controller/import_task_api_controller.dart';
import 'package:bio_clean/api/api_controller/meet_api_controller.dart';
import 'package:bio_clean/api/api_model/import_task_model.dart';
import 'package:bio_clean/api/api_model/meet_model.dart';
import 'package:bio_clean/utils/helpers.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ImportTaskGetsController extends GetxController with Helpers {
  RxList<ImportTask> imports = <ImportTask>[].obs;
  ImportTaskApiController _importApiController = ImportTaskApiController();
  static ImportTaskGetsController get to=>Get.find();

  @override
  void onInit() {
    // TODO: implement onInit
    getImportTask();
    super.onInit();
  }

  Future<void> getImportTask() async {
    imports.value = await _importApiController.getImportTask();
  }

  Future<bool> crateImportTask(
      {required BuildContext context, required ImportTask import}) async {
    ImportTask? newImport =
    await _importApiController.createImportTask(context: context, import: import);
    if (newImport != null) {
      imports.add(newImport);
      return true;
    }

    return newImport != null;
  }

  Future<bool> updateImportTask(
      {required BuildContext context,
        required ImportTask import}) async {
    bool updated =
    await _importApiController.updateImportTask(context: context, import: import);
    if (updated) {
      int index = imports.indexWhere((element) => element.id == import.id);
      if (index != -1) {
        imports[index] = import;
      }
    }
    return updated;
  }

  Future<bool> deleteImportTask(
      {required BuildContext context, required int index}) async {
    bool deleted = await _importApiController.deleteImportTask(
        context: context, id: imports[index].id);
    if (deleted) {
      imports.removeAt(index);
      showSnackBar(context: context, message: 'تم حذف المهمة');
    }else{
      showSnackBar(context: context, message: 'تعذرت عملية حذف المهمة',error: true);
    }
    return deleted;
  }
}
