import 'package:bio_clean/models/student.dart';
import 'package:shared_preferences/shared_preferences.dart';

class StudentPref {
  static final StudentPref _instance = StudentPref._();
  late SharedPreferences _sharedPreferences;

  factory StudentPref() {
    return _instance;
  }

  StudentPref._();

  Future<void> initPreferences() async {
    _sharedPreferences = await SharedPreferences.getInstance();
  }

  Future<void> save({required Student student}) async {
    await _sharedPreferences.setBool('logged_in', true);
    await _sharedPreferences.setInt('id', student.id);
    await _sharedPreferences.setString('fullName', student.fullName);
    await _sharedPreferences.setString('email', student.email);
    await _sharedPreferences.setString('gender', student.gender);
    await _sharedPreferences.setString('token', 'Bearer ${student.token}');
  }

  String get token => _sharedPreferences.getString('token') ?? '';

  bool get loggedIn => _sharedPreferences.getBool('logged_in') ?? false;

  Future<bool> logout() async {
    return await _sharedPreferences.clear();
  }
}
