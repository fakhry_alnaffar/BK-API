import 'dart:io';

import 'package:bio_clean/preferences/student_pref.dart';
import 'package:flutter/material.dart';

import 'helpers.dart';

class ApiHelpers with Helpers{
  Map <String,String>get headers{
   return{
   HttpHeaders.authorizationHeader:StudentPref().token,
   HttpHeaders.acceptHeader:'application/json'
   } ;
  }
}