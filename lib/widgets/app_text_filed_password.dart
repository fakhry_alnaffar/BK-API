import 'package:bio_clean/responsive/size_config.dart';
import 'package:flutter/material.dart';

class AppTextFiledPassword extends StatelessWidget {
  final TextInputType textInputType;
  final String labelText;
  final int? maxLength;
  final TextEditingController controller;
  final bool obscureText;
  final bool readOnly;
  final bool showCursor;
  Function? functionSuffixPressed;
  final IconData? suffix;
  final IconData? prefix;
  Function()? onTap;
  Function(String? value)? validator;


  AppTextFiledPassword({
    this.textInputType = TextInputType.text,
    required this.labelText,
    this.maxLength,
    this.suffix = null,
    this.prefix = null,
    this.obscureText = false,
    this.readOnly = false,
    this.showCursor = true,
    required this.controller,
    this.onTap,
    this.validator(String? value)?,
    this.functionSuffixPressed
  });

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      validator: (String? value) {
        validator;
      },
      controller: controller,
      maxLength: maxLength,
      showCursor: showCursor,
      cursorColor: Color(0xff5A55CA),
      autocorrect: true,
      enableSuggestions: true,
      readOnly: readOnly,
      keyboardType: textInputType,
      obscureText: obscureText,
      onTap: onTap,
      decoration: InputDecoration(
        labelText: labelText,
        labelStyle: TextStyle(color: Color(0xff4947a5)),
        counterText: '',
        enabledBorder: borderEnable,
        focusedBorder: borderFocused,
        suffixIcon: suffix != null ? IconButton(onPressed: () {
          functionSuffixPressed!();
        }, icon: Icon(suffix, color: Color(0xff5A55CA),)) : null,
        prefixIcon: Icon(prefix, color: Color(0xff5A55CA),),
      ),
    );
  }

  OutlineInputBorder get borderEnable =>
      OutlineInputBorder(
        borderRadius: BorderRadius.circular(SizeConfig().scaleWidth(10),),
        borderSide: BorderSide(
          color: Color(0xffb7b5e5),
          width: SizeConfig().scaleWidth(2),
        ),
      );

  OutlineInputBorder get borderFocused =>
      OutlineInputBorder(
        borderRadius: BorderRadius.circular(SizeConfig().scaleWidth(10),),
        borderSide: BorderSide(
          color: Color(0xff5A55CA),
          width: SizeConfig().scaleWidth(2),
        ),
      );
}
