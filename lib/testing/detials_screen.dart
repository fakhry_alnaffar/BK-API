import 'package:bio_clean/testing/button.dart';
import 'package:bio_clean/testing/product.dart';
import 'package:bio_clean/testing/product_color.dart';
import 'package:bio_clean/testing/product_count.dart';
import 'package:bio_clean/testing/product_size.dart';
import 'package:flutter/material.dart';

class DetailsScreen extends StatefulWidget {
  Product? productDetails;

  // DetailsScreen(this.productDetails);

  @override
  _DetailsScreenState createState() => _DetailsScreenState();
}

class _DetailsScreenState extends State<DetailsScreen> {
  late Color _colorOrange = Colors.deepOrange;
  late Color _colorYellow = Colors.yellow;
  late Color _colorBlue = Colors.lightBlue;
  late Color _colorPink = Colors.pink;

  late Color _colorS = Colors.grey.shade200;
  late Color _colorM = Colors.grey.shade200;
  late Color _colorL = Colors.grey.shade200;
  late Color _colorXL = Colors.grey.shade200;

  late bool _isClickOrange = false;
  late bool _isClickYellow = false;
  late bool _isClickBlue = false;
  late bool _isClickPink = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey.shade100,
      body: Stack(
        children: [
          Container(
            width: double.infinity,
            height: double.infinity,
          ),
          // Container(
          //   width: double.infinity,
          //   height: 450,
          //   child: Image.network(
          //     widget.productDetails!.image,
          //     fit: BoxFit.cover,
          //   ),
          // ),
          Positioned(
            left: 0,
            right: 0,
            bottom: 0,
            child: Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(20),
                  topRight: Radius.circular(
                    20,
                  ),
                ),
              ),
              child: Padding(
                padding: EdgeInsets.all(20),
                child: Column(
                  children: [
                    Row(
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            // Text(
                            //   widget.productDetails!.name,
                            //   style: TextStyle(
                            //     fontSize: 18,
                            //     fontWeight: FontWeight.bold,
                            //   ),
                            // ),
                            Text('Rate')
                          ],
                        ),
                        Spacer(),
                        // Text(
                        //   widget.productDetails!.price.toString(),
                        //   style: TextStyle(
                        //     fontSize: 18,
                        //     fontWeight: FontWeight.bold,
                        //   ),
                        // ),
                      ],
                    ),
                    SizedBox(height: 30),
                    Row(
                      children: [
                        Text(
                          'Size: ',
                          style: TextStyle(
                            fontSize: 17,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        Spacer(),
                        Row(
                          children: [
                            InkWell(
                              child: ProductSize(
                                title: 'S',
                                color: _colorS,
                              ),
                              onTap: () {
                                setState(() {
                                  _colorS = Colors.deepOrange;
                                  _colorM = Colors.grey.shade200;
                                  _colorL = Colors.grey.shade200;
                                  _colorXL = Colors.grey.shade200;
                                });
                              },
                            ),
                            SizedBox(width: 10),
                            InkWell(
                              child: ProductSize(
                                title: 'M',
                                color: _colorM,
                              ),
                              onTap: (){
                                setState(() {
                                  _colorM = Colors.deepOrange;
                                  _colorS = Colors.grey.shade200;
                                  _colorL = Colors.grey.shade200;
                                  _colorXL = Colors.grey.shade200;
                                });
                              },
                            ),
                            SizedBox(width: 10),
                            InkWell(
                              child: ProductSize(
                                title: 'L',
                                color: _colorL,
                              ),
                              onTap: (){
                                setState(() {
                                  _colorL = Colors.deepOrange;
                                  _colorS = Colors.grey.shade200;
                                  _colorM = Colors.grey.shade200;
                                  _colorXL = Colors.grey.shade200;
                                });
                              },
                            ),
                            SizedBox(width: 10),
                            InkWell(
                              child: ProductSize(
                                title: 'XL',
                                color: _colorXL,
                              ),
                              onTap: (){
                                setState(() {
                                  _colorXL = Colors.deepOrange;
                                  _colorS = Colors.grey.shade200;
                                  _colorM = Colors.grey.shade200;
                                  _colorL = Colors.grey.shade200;
                                });
                              },
                            ),

                          ],
                        ),
                      ],
                    ),
                    SizedBox(height: 30),
                    Row(
                      children: [
                        Text(
                          'Color: ',
                          style: TextStyle(
                              fontSize: 17, fontWeight: FontWeight.bold),
                        ),
                        Spacer(),
                        Row(
                          children: [
                            InkWell(
                              child: ProductColor(
                                color: _colorOrange,
                                isCheck: _isClickOrange,
                              ),
                              onTap: () {
                                print('setState');
                                setState(() {
                                  _isClickOrange = !_isClickOrange;
                                  if (_isClickOrange == true) {
                                    _isClickPink = false;
                                    _isClickYellow = false;
                                    _isClickBlue = false;
                                  }
                                });
                              },
                            ),
                            SizedBox(width: 10),
                            InkWell(
                              child: ProductColor(
                                color: _colorYellow,
                                isCheck: _isClickYellow,
                              ),
                              onTap: () {
                                print('setState');
                                setState(() {
                                  _isClickYellow = !_isClickYellow;
                                  if (_isClickYellow == true) {
                                    _isClickPink = false;
                                    _isClickOrange = false;
                                    _isClickBlue = false;
                                  }
                                });
                              },
                            ),
                            SizedBox(width: 10),
                            InkWell(
                              child: ProductColor(
                                color: _colorBlue,
                                isCheck: _isClickBlue,
                              ),
                              onTap: () {
                                print('setState');
                                setState(() {
                                  _isClickBlue = !_isClickBlue;
                                  if (_isClickBlue == true) {
                                    _isClickPink = false;
                                    _isClickOrange = false;
                                    _isClickYellow = false;
                                  }
                                });
                              },
                            ),
                            SizedBox(width: 10),
                            InkWell(
                              child: ProductColor(
                                color: _colorPink,
                                isCheck: _isClickPink,
                              ),
                              onTap: () {
                                print('setState');
                                setState(() {
                                  _isClickPink = !_isClickPink;
                                  if (_isClickPink == true) {
                                    _isClickBlue = false;
                                    _isClickOrange = false;
                                    _isClickYellow = false;
                                  }
                                });
                              },
                            ),
                          ],
                        )
                      ],
                    ),
                    SizedBox(height: 30),
                    Row(
                      children: [
                        Text(
                          'Quantity: ',
                          style: TextStyle(
                              fontSize: 17, fontWeight: FontWeight.bold),
                        ),
                        Spacer(),
                        Row(
                          children: [
                            ProductCount(
                              icon: Icons.add,
                            ),
                            SizedBox(width: 10),
                            Text('1'),
                            SizedBox(width: 10),
                            ProductCount(
                              icon: Icons.minimize,
                            ),
                          ],
                        )
                      ],
                    ),
                    SizedBox(height: 30),
                    SizedBox(
                      width: double.infinity,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'Description',
                            style: TextStyle(
                                fontSize: 17, fontWeight: FontWeight.bold),
                          ),
                          // Text(
                          //   widget.productDetails!.description,
                          //   style: TextStyle(fontSize: 15, color: Colors.grey),
                          // ),
                        ],
                      ),
                    ),
                    SizedBox(height: 30),
                    Button(
                      color: Colors.deepOrange,
                      value: 'Add To Cart',
                      colorValue: Colors.white,
                      onPressed: () {},
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
/*
*
* _colorS == Colors.grey.shade200 &&
                                      _colorM == Colors.grey.shade200 &&
                                      _colorL == Colors.grey.shade200 &&
                                      _colorXL == Colors.grey.shade200
* */
