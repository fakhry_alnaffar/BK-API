import 'package:bio_clean/screen/auth/change_password.dart';
import 'package:bio_clean/screen/auth/home_screen.dart';
import 'package:bio_clean/screen/auth/launch_screen.dart';
import 'package:bio_clean/screen/employee/add_employee.dart';
import 'package:bio_clean/screen/employee/employee_screen.dart';
import 'package:bio_clean/screen/import_task/import_task_screen.dart';
import 'package:bio_clean/screen/login/login_screen.dart';
import 'package:bio_clean/screen/meet/add_meet.dart';
import 'package:bio_clean/screen/meet/meeet_screen.dart';
import 'package:bio_clean/screen/profile/profile_screen.dart';
import 'package:bio_clean/screen/task/add_task_screen.dart';
import 'package:bio_clean/screen/task/task_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  // await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Locale _locale = Locale('ar');
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      // home: LoginScreen(),
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      supportedLocales: [
        Locale('ar'),
      ],
      locale: _locale,
      initialRoute: 'launch_screen',
      routes: {
        'launch_screen': (context) => LaunchScreen(),
        'login_screen': (context) => LoginScreen(),
        'home_screen': (context) => HomeScreen(),
        'add_meet': (context) => AddMeetScreen(),
        'meet_screen': (context) => MeetScreen(),
        'add_task': (context) => AddTask(),
        'task_screen': (context) => TaskScreen(),
        'employee_screen': (context) => EmployeeScreen(),
        'add_employee': (context) => AddEmployeeScreen(),
        'profile_screen': (context) => ProfileScreen(),
        'change_password': (context) => ChangePassword(),
        'import_task': (context) => ImportScreen(),
      },
    );
  }
}
