import 'dart:io';
import 'package:bio_clean/utils/helpers.dart';
import 'package:flutter/material.dart';

class ApiHelpers with Helpers{
  Map <String,String>get headers{
    return{
      // HttpHeaders.authorizationHeader:StudentPref().token,
      HttpHeaders.acceptHeader:'application/json'
    } ;
  }
}