class ApiSetting{
  static const _BASE_URL = 'https://task-management-11.herokuapp.com/';
  static const _BASE_API_URL = _BASE_URL + 'api/';
  static const TASKS = _BASE_API_URL + 'tasks';
  static const ShowTASKS = 'http://task-management-11.herokuapp.com/api/tasks';
  static const ShowMeet = 'https://task-management-11.herokuapp.com/api/category/meeting-task';
  static const UPDATE_MEET = 'http://task-management-11.herokuapp.com/api/tasks/';
  static const UPDATE2_MEET = '/update';
  static const ShowImportTask = 'https://task-management-11.herokuapp.com/api/category/import-task';
  static const Create_IMPORT_TAST = 'http://task-management-11.herokuapp.com/api/tasks/meeting-taskk';
  static const UPDATE_IMPORT_TASK = 'http://task-management-11.herokuapp.com/api/tasks/';
  static const UPDATE2_IMPORT_TASK  = '/update';
  static const CreateMeet = 'http://task-management-11.herokuapp.com/api/tasks/meeting-taskk';
  static const SHOW_TASK = 'http://task-management-11.herokuapp.com/api/category/general-task';
  static const CREATE_TASK = 'http://task-management-11.herokuapp.com/api/tasks/general-task';
  static const UPDATE_TASK = 'http://task-management-11.herokuapp.com/api/tasks/';
  static const UPDATE2_TASK = '/update';
  static const DELETE_TASK = 'http://task-management-11.herokuapp.com/api/tasks/';
  static const DELETE2_TASK = '/delete';
  static const DELETE_USER = 'http://task-management-11.herokuapp.com/api/users/';
  static const DELETE2_USER = '/delete';
  static const SHOW_ALL_USER = 'http://task-management-11.herokuapp.com/api/users';
  static const CREATE_USER = 'http://task-management-11.herokuapp.com/api/users/create';
  static const UPDATE_USER = 'http://task-management-11.herokuapp.com/api/users/';
  static const UPDATE2_USER = '/update';
}