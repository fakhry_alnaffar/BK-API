class Pivot {
   int? taskId;
   int? userId;
  late String createdAt;
  late String updatedAt;

  Pivot.fromJson(Map<String, dynamic> json) {
    taskId = json['task_id'];
    userId = json['user_id'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['task_id'] = this.taskId;
    data['user_id'] = this.userId;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}
