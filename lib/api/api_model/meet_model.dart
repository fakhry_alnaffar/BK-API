class Meet {
 late int id;
 late String? name;
 late String? description;
 late String? time;
 late String? date;
 late int? categoryId;
 late String createdAt;
 late String updatedAt;
 Meet();

  Meet.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    description = json['description'];
    time = json['time'];
    date = json['date'];
    categoryId = json['category_id'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['description'] = this.description;
    data['time'] = this.time;
    data['date'] = this.date;
    data['category_id'] = this.categoryId;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}