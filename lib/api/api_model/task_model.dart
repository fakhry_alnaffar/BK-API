import 'package:bio_clean/api/api_model/user_data_name.dart';

class Task {
 int id=0;
 String? name='';
 String? description='';
 String? time='';
 String? date='';
 late int categoryId;
 String? createdAt='';
 String? updatedAt='';
 String? status='';
List<String>? userId=<String>[];

  Task();

  Task.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    description = json['description'];
    time = json['time'];
    date = json['date'];
    categoryId = json['category_id'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    status = json['status'];
    userId = json['user_id'];
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['description'] = this.description;
    data['time'] = this.time;
    data['date'] = this.date;
    data['category_id'] = '1'.toString();
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['status'] = this.status;
    data['user_id[0]'] = '0'.toString();
    return data;
  }
}
