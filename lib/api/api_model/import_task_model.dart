class ImportTask {
late  int id;
late  String? name;
late  String? description;
late  String? time;
late  String? date;
late  int? categoryId;
late  String? createdAt;
late  String? updatedAt;
late  String? status;
late  String? secureCheck;
late  int? sendToSincere;
late  String? createdCertification;
late  String? invoice;
late  String? packingList;
late  String? driverIsrael;
late  String? driverGaza;
late  String? deletedAt;
 ImportTask();

 ImportTask.fromJson(Map<String, dynamic> json) {
   id = json['id'];
   name = json['name'];
   description = json['description'];
   time = json['time'];
   date = json['date'];
   categoryId = json['category_id'];
   createdAt = json['created_at'];
   updatedAt = json['updated_at'];
   status = json['status'];
   secureCheck = json['secure_check'];
   sendToSincere = json['Send_to_sincere'];
   createdCertification = json['created_certification'];
   invoice = json['invoice'];
   packingList = json['packing_list'];
   driverIsrael = json['driver_israel'];
   driverGaza = json['driver_gaza'];
   deletedAt = json['deleted_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['description'] = this.description;
    data['time'] = this.time;
    data['date'] = this.date;
    data['category_id'] = this.categoryId;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['status'] = this.status;
    data['secure_check'] = this.secureCheck;
    data['Send_to_sincere'] = this.sendToSincere;
    data['created_certification'] = this.createdCertification;
    data['invoice'] = this.invoice;
    data['packing_list'] = this.packingList;
    data['driver_israel'] = this.driverIsrael;
    data['driver_gaza'] = this.driverGaza;
    data['deleted_at'] = this.deletedAt;
    return data;
  }
}