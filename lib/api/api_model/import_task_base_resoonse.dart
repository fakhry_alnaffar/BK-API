import 'package:bio_clean/api/api_model/import_task_model.dart';

class ImportTaskBaseResponse {
 late List<ImportTaskBaseResponse> data;
 late int status;
 late String msg;


  ImportTaskBaseResponse.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data =  <ImportTaskBaseResponse>[];
      json['data'].forEach((v) {
        data.add( ImportTaskBaseResponse.fromJson(v));
      });
    }
    status = json['status'];
    msg = json['msg'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    data['status'] = this.status;
    data['msg'] = this.msg;
    return data;
  }
}