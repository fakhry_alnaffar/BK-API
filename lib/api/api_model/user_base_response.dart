import 'package:bio_clean/api/api_model/meet_model.dart';
class UserBaseResponse {
  late List<UserBaseResponse> data;
  late int status;


  UserBaseResponse.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data =<UserBaseResponse>[];
      json['data'].forEach((v) {
        data.add(new UserBaseResponse.fromJson(v));
      });
    }
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    data['status'] = this.status;
    return data;
  }
}