import 'dart:convert';

import 'package:bio_clean/api/helper/api_helpers.dart';
import 'package:bio_clean/api/api_model/meet_model.dart';
import 'package:bio_clean/api/helper/api_setting.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class MeetApiController extends ApiHelpers {
  Future<List<Meet>> getMeet({BuildContext? context}) async {
    var url = Uri.parse(ApiSetting.ShowMeet);
    print(url);
    var response = await http.get(url, headers: headers);
    if (response.statusCode == 200) {
      var taskJsonArray = jsonDecode(response.body)['data'] as List;
      return taskJsonArray
          .map((taskJsonObject) => Meet.fromJson(taskJsonObject))
          .toList();
    } else if (response.statusCode != 500) {
      // showMessage(context, jsonDecode(response.body)['message'], error: true);
    } else {
      showMessage(context, 'Something went wrong, Please try again!',
          error: true);
    }
    return [];
  }
  Future<int>getMeetCount()async{
    var url = Uri.parse(ApiSetting.ShowMeet);
    print(url);
    var response = await http.get(url, headers: headers);
    var taskJsonArray = jsonDecode(response.body)['data'] as List;
    return taskJsonArray.length;

  }

  Future<Meet?> createMeet(
      {required BuildContext context, required Meet meet}) async {
    var url = Uri.parse('http://task-management-11.herokuapp.com/api/tasks/meeting-task');
    print('CREATE URL $url');
    var response = await http.post(url, body: {
      'name': meet.name,
      'description': meet.description,
      'time': meet.time,
      'date': meet.date,
      'category_id': '1'.toString(),
      'status': '2'.toString(),
      'user_id[0]': '2'.toString(),
    });
    if (response.statusCode == 200) {
      //SUCCESS
      var meetJsonObject = jsonDecode(response.body)['data'];
      // showMessage(context, jsonDecode(response.body)['message']);
      showSnackBar(context: context, message: 'SUCCESS CREATED', error: false);

      return Meet.fromJson(meetJsonObject);
    } else if (response.statusCode != 500) {
      showSnackBar(context: context, message: 'ERROR CREATED', error: true);
    } else if (response.statusCode==500) {
      showSnackBar(
          context: context,
          message: 'Something went wrong, Please try again!',
          error: true);
    }
    return null;
  }

  Future<bool> updateMeet(
      {required BuildContext context, required Meet meet}) async {
    var url = Uri.parse(
        ApiSetting.UPDATE_MEET + '${meet.id}' + ApiSetting.UPDATE2_MEET);
    var response = await http.put(url, headers: headers, body: {
      'name': meet.name,
      'description': meet.description,
      'time': meet.time,
      'date': meet.date,
    });
    if (response.statusCode == 200) {
      //SUCCESS
      showMessage(context, 'SUCCESS UPDATE');
      return true;
    } else if (response.statusCode != 500) {
      // showMessage(context, jsonDecode(response.body)['message'], error: true);
      showMessage(context, 'FAILED UPDATE', error: true);
    } else {
      showSnackBar(
          context: context,
          message: 'Something went wrong, Please try again!',
          error: true);
    }
    return false;
  }

  Future<bool> deleteMeet(
      {required BuildContext context, required int id}) async {
    var url = Uri.parse(ApiSetting.TASKS + '/$id/delete');
    print(url);
    var response = await http.delete(
      url,
      headers: headers,
    );
    if (response.statusCode == 200) {
      //SUCCESS
      // showMessage(context, jsonDecode(response.body)['message']);
      return true;
    } else if (response.statusCode != 500) {
      // showMessage(context, jsonDecode(response.body)['message'], error: true);
    } else {
      showSnackBar(
          context: context,
          message: 'Something went wrong, Please try again!',
          error: true);
    }
    return false;
  }
}
