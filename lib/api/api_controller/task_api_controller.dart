import 'dart:convert';

import 'package:bio_clean/api/api_model/task_model.dart';
import 'package:bio_clean/api/helper/api_helpers.dart';
import 'package:bio_clean/api/helper/api_setting.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class TaskApiController extends ApiHelpers {
  Future<List<Task>> getTask({BuildContext? context}) async {
    var url = Uri.parse('https://task-management-11.herokuapp.com/api/category/general-task');
    print(url);
    var response = await http.get(url, headers: headers);
    if (response.statusCode == 200) {
      var taskJsonArray = jsonDecode(response.body)['data'] as List;
      return taskJsonArray
          .map((taskJsonObject) => Task.fromJson(taskJsonObject))
          .toList();
    } else if (response.statusCode != 500) {
      // showMessage(context, jsonDecode(response.body)['message'], error: true);
    } else {
      showMessage(context, 'Something went wrong, Please try again!',
          error: true);
    }
    return [];
  }

  Future<int>getTaskCount()async{
    var url = Uri.parse(ApiSetting.SHOW_TASK);
    print(url);
    var response = await http.get(url, headers: headers);
    var taskJsonArray = jsonDecode(response.body)['data'] as List;
   return taskJsonArray.length;

  }

  Future<Task?> createTask(
      {required BuildContext context, required Task task}) async {
    var url = Uri.parse('http://task-management-11.herokuapp.com/api/tasks/general-task');
    print('CREATE URL $url');
    var response = await http.post(url, body: {
      'name': task.name,
      'description': task.description,
      'time': task.time,
      'date': task.date,
      'category_id': '1'.toString(),
      'status':'1'.toString(),
      'user_id[0]': '3'.toString(),
    });
    print(task.description);
    print(task.name);
    print(task.time);
    print(task.date);

    if (response.statusCode == 200) {
      //SUCCESS
      var meetJsonObject = jsonDecode(response.body)['data'];
      // showMessage(context, jsonDecode(response.body)['message']);
      showSnackBar(context: context, message: 'SUCCESS CREATED', error: false);
      return Task.fromJson(meetJsonObject);
    } else if (response.statusCode != 500) {
      // var meetJsonObject = jsonDecode(response.body)['data'];
      showSnackBar(context: context, message: 'ERROR CREATED', error: true);
    } else {
      showSnackBar(
          context: context,
          message: 'SUCCESS CREATED',
          error: false);
      Navigator.pop(context);
    }
    return null;
  }

  Future<bool> updateTask(
      {required BuildContext context, required Task task}) async {
    var url = Uri.parse(
        ApiSetting.UPDATE_TASK + '${task.id}' + ApiSetting.UPDATE2_TASK);
    var response = await http.put(url, headers: headers, body: {
      'name': task.name,
      'description': task.description,
      'time': task.time,
      'date': task.date,
    });
    if (response.statusCode == 200) {
      //SUCCESS
      showMessage(context, 'SUCCESS UPDATE');
      return true;
    } else if (response.statusCode != 500) {
      // showMessage(context, jsonDecode(response.body)['message'], error: true);
      showMessage(context, 'FAILED UPDATE', error: true);
    } else {
      showSnackBar(
          context: context,
          message: 'Something went wrong, Please try again!',
          error: true);
    }
    return false;
  }

  Future<bool> deleteTask(
      {required BuildContext context, required int id}) async {
    var url =
        Uri.parse(ApiSetting.DELETE_TASK + '$id' + ApiSetting.DELETE2_TASK);
    print(url);
    var response = await http.delete(
      url,
      headers: headers,
    );
    if (response.statusCode == 200) {
      //SUCCESS
      // showMessage(context, jsonDecode(response.body)['message']);
      return true;
    } else if (response.statusCode != 500) {
      // showMessage(context, jsonDecode(response.body)['message'], error: true);
    } else {
      showSnackBar(
          context: context,
          message: 'Something went wrong, Please try again!',
          error: true);
    }
    return false;
  }
}
