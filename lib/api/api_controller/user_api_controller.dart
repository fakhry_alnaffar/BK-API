import 'dart:convert';

import 'package:bio_clean/api/api_model/user_model.dart';
import 'package:bio_clean/api/helper/api_helpers.dart';
import 'package:bio_clean/api/helper/api_setting.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class UserApiController extends ApiHelpers {
  Future<List<UserData>> getUser({BuildContext? context}) async {
    var url = Uri.parse(ApiSetting.SHOW_ALL_USER);
    print(url);
    var response = await http.get(url, headers: headers);
    if (response.statusCode == 200) {
      var userJsonArray = jsonDecode(response.body)['data'] as List;
      return userJsonArray
          .map((taskJsonObject) => UserData.fromJson(taskJsonObject))
          .toList();
    } else if (response.statusCode != 500) {
      // showMessage(context, jsonDecode(response.body)['message'], error: true);
    } else {
      showMessage(context, 'Something went wrong, Please try again!',
          error: true);
    }
    return [];
  }

  Future<int>getUserCount()async{
    var url = Uri.parse(ApiSetting.SHOW_ALL_USER);
    print(url);
    var response = await http.get(url, headers: headers);
    var taskJsonArray = jsonDecode(response.body)['data'] as List;
   return taskJsonArray.length;

  }

  Future<UserData?> createUser(
      {required BuildContext context, required UserData user}) async {
    var url = Uri.parse(ApiSetting.CREATE_USER);
    print('CREATE URL $url');
    var response = await http.post(url, body: {
      'name': user.name,
      'email': user.email,
      'phone': user.phone,
      'password': '123123',
      'image':'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRPtENZS1gdjgiNbiX6gLEZPDCiKUXSkSo9SY4ZnWPxGwESsCxZeoXUOIQiFsD8ph-WmAc&usqp=CAU',
    });
    if (response.statusCode == 200) {
      //SUCCESS
      var userJsonObject = jsonDecode(response.body)['data'];
      // showMessage(context, jsonDecode(response.body)['message']);
      showSnackBar(context: context, message: 'SUCCESS CREATED', error: false);

      return UserData.fromJson(userJsonObject);
    } else if (response.statusCode != 500) {
      showSnackBar(context: context, message: 'ERROR CREATED', error: true);
    } else {
      showSnackBar(
          context: context,
          message: 'Something went wrong, Please try again!',
          error: true);
    }
    return null;
  }

  Future<bool> updateUser(
      {required BuildContext context, required UserData user}) async {
    var url = Uri.parse(ApiSetting.UPDATE_USER+'${user.id}'+ApiSetting.UPDATE2_USER);
    var response = await http.put(url, headers: headers, body: {
      'name': user.name,
      'email': user.email,
      'phone': user.phone,
      'password': '123123',
      'image':'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRPtENZS1gdjgiNbiX6gLEZPDCiKUXSkSo9SY4ZnWPxGwESsCxZeoXUOIQiFsD8ph-WmAc&usqp=CAU',
    });
    if (response.statusCode == 200) {
      //SUCCESS
      showMessage(context, 'SUCCESS UPDATE');
      return true;
    } else if (response.statusCode != 500) {
      // showMessage(context, jsonDecode(response.body)['message'], error: true);
      showMessage(context, 'FAILED UPDATE', error: true);
    } else {
      showSnackBar(
          context: context,
          message: 'Something went wrong, Please try again!',
          error: true);
    }
    return false;
  }
  Future<bool> deleteUser(
      {required BuildContext context, required int id}) async {
    var url = Uri.parse('http://task-management-11.herokuapp.com/api/users/$id/delete');
    print(url);
    var response = await http.delete(
      url,
      headers: headers,
    );
    if (response.statusCode == 200) {
      //SUCCESS
      showMessage(context, jsonDecode(response.body)['message']);
      return true;
    } else if (response.statusCode != 500) {
      showMessage(context, jsonDecode(response.body)['message'], error: true);
    } else {
      showSnackBar(
          context: context,
          message: 'Something went wrong, Please try again!',
          error: true);
    }
    return false;
  }
}
